<?php 
/*
Plugin Name: Bitcot Apps Data & Email Marketing
Plugin URI: https://bitcot.com
Description: Bitcot shopify and bigcommerce app data and email marketing tool for personal use.
Version: 0.0.1
Author: BitCot (Nilesh)
Author URI: https://bitcot.com
Text Domain: badem
*/

/*If this file is called directly, abort.*/
if ( ! defined( 'WPINC' ) ) {die;} /*end if*/
if ( ! defined( 'ABSPATH' ) ) { exit; } /*Exit if accessed directly.*/

/*Let's Initialize Everything*/
if ( file_exists( plugin_dir_path( __FILE__ ) . 'core-init.php' ) ) {
require_once( plugin_dir_path( __FILE__ ) . 'core-init.php' );
}
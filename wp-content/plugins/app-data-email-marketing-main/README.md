# app-data-email-marketing
Hubify App Data Plugin

# Get token api 
curl --location --request POST 'http://server.domain/wp-json/jwt-auth/v1/token?username=username&password=password' \
--data-raw ''


# Subscribe or add user in mail list of mailpoet
curl --location --request POST 'http://server.domain/wp-json/maillist/v1/add-user' \
--header 'Authorization: Bearer {token_key}' \
--header 'Content-Type: text/plain' \
--data-raw '{"first_name":"john", "last_name":"cena","email":"john@example.com"}'


# un-subscribe/remove user from mail list of mailpoet
curl --location --request POST 'http://server.domain/wp-json/maillist/v1/unsubscribe-user' \
--header 'Authorization: Bearer {token_key}' \
--header 'Content-Type: text/plain' \
--data-raw '{"email":"john@example.com"}'

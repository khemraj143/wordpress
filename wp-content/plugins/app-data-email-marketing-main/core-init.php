<?php 

/*****
	Description: Ajax handlers file for handling js code without page refresh.
	Author: BitCot
	Created: 30-Mar-2021
	Modify:

*****/

/*If this file is called directly, abort.*/
if ( ! defined( 'WPINC' ) ) {die;} /*end if*/

if ( ! defined( 'ABSPATH' ) ) { exit; } /* Exit if accessed directly.*/

/*Define Our Constants*/
$url = plugin_dir_url(__FILE__);
define('BADEM_PLUGIN_URL',$url);
define('BADEM_PLUGIN_DIR', plugin_dir_path( __FILE__ ));

define('BADEM_CORE_INC',dirname( __FILE__ ).'/assets/inc/');
define('BADEM_CORE_IMG',plugins_url( 'assets/img/', __FILE__ ));
define('BADEM_CORE_CSS',plugins_url( 'assets/css/', __FILE__ ));
define('BADEM_CORE_JS',plugins_url( 'assets/js/', __FILE__ ));
define('BADEM_CORE_PG',plugins_url( 'assets/css/pagination', __FILE__ ));

/*
*
*  Load the File Including Activation hook to create data tables in database.
*
*/
if ( file_exists( BADEM_PLUGIN_DIR . 'table-create.php' ) ) {
	require_once BADEM_PLUGIN_DIR . 'table-create.php';
}

/*
*
*  Register CSS
*
*/
function badem_register_core_css(){
	wp_enqueue_style('badem-core', BADEM_CORE_CSS . 'badem-core.css',null,time(),'all' );
	wp_register_style( 'badem-dataTables', BADEM_CORE_CSS .  'jquery.dataTables.min.css', array(), time(), 'all' );
	wp_register_style( 'badem-dataTables-responsive', BADEM_CORE_CSS .  'responsive.dataTables.min.css', array(), time(), 'all' );
	wp_register_style( 'badem-dataTables-rowReorder', BADEM_CORE_CSS .  'rowReorder.dataTables.min.css', array(), time(), 'all' );
	wp_register_style( 'badem-bootstrap-min', BADEM_CORE_CSS .  'bootstrap.min.css', array(), time(), 'all' );
	wp_register_style( 'badem-font-awesome', BADEM_CORE_CSS .  'font-awesome.min.css', array(), time(), 'all' );
	wp_register_style( 'badem-jquery-ui-css', BADEM_CORE_CSS .  'jquery-ui.css', array(), time(), 'all' );
};
/*add_action( 'wp_enqueue_scripts', 'badem_register_core_css' );*/    
add_action( 'admin_enqueue_scripts', 'badem_register_core_css' );    
/*
*
*  Register JS/Jquery Ready
*
*/
function add_e2_date_picker(){
    //jQuery UI date picker file
    wp_enqueue_script('jquery-ui-datepicker');
    //jQuery UI theme css file
    wp_enqueue_style('e2b-admin-ui-css','http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/themes/base/jquery-ui.css',false,"1.9.0",false);
    }
    add_action('admin_enqueue_scripts', 'add_e2_date_picker'); 

function badem_register_core_js(){
	/*Register Core Plugin JS*/	
	wp_enqueue_script('badem-core', BADEM_CORE_JS . 'badem-core.js','jquery',time(),true);
	wp_localize_script( 'badem-core', 
		'badem_object',
            array( 
            	'ajax_url' => admin_url( 'admin-ajax.php' ), 
            	'plugin_url' =>  BADEM_CORE_IMG
            ) 
        );
	
	wp_register_script( 'badem-datatables', BADEM_CORE_JS . 'jquery.dataTables.min.js', array('jquery'), time(), false );
	wp_register_script( 'badem-datatables-roworder', BADEM_CORE_JS . 'dataTables.rowReorder.min.js', array('jquery'), time(), false );
	wp_register_script( 'badem-datatables-responsive', BADEM_CORE_JS . 'dataTables.responsive.min.js', array('jquery'), time(), false );
	wp_register_script( 'badem-popper', BADEM_CORE_JS . 'popper.min.js', array('jquery'), time(), false );
	wp_register_script( 'badem-bootstrap-min', BADEM_CORE_JS . 'bootstrap.min.js', array('jquery'), time(), false );
	wp_register_script( 'export_query_min', BADEM_CORE_JS . 'export_query_min.js', array('jquery'), time(), false );
	wp_register_script( 'jquery-jqplot-js', BADEM_CORE_JS . 'jqplot/jquery.jqplot.js', array('jquery'), time(), false );
	wp_register_script( 'jqplot-highlighter-js', BADEM_CORE_JS . 'jqplot/jqplot.highlighter.js', array('jquery'), time(), false );
	wp_register_script( 'jqplot-cursor-js', BADEM_CORE_JS . 'jqplot/jqplot.cursor.js', array('jquery'), time(), false );
	wp_register_script( 'jqplot-dateAxisRenderer-js', BADEM_CORE_JS . 'jqplot/jqplot.dateAxisRenderer.js', array('jquery'), time(), false );
	wp_register_script( 'jqplot-pieRenderer-js', BADEM_CORE_JS . 'jqplot/jqplot.pieRenderer.min.js', array('jquery'), time(), false );

};
/*add_action( 'wp_enqueue_scripts', 'badem_register_core_js' );*/    
add_action( 'admin_enqueue_scripts', 'badem_register_core_js' );    
/*
*
*  Includes
*
*/ 

/*Load the Functions*/
if ( file_exists( BADEM_CORE_INC . 'badem-core-functions.php' ) ) {
	require_once BADEM_CORE_INC . 'badem-core-functions.php';
}

/*Load the Rest API Functions*/
if ( file_exists( BADEM_CORE_INC . 'badem-rest-api-functions.php' ) ) {
	require_once BADEM_CORE_INC . 'badem-rest-api-functions.php';
}


/*Load the ajax Request*/
if ( file_exists( BADEM_CORE_INC . 'ajax/badem-ajax-request.php' ) ) {
	require_once BADEM_CORE_INC . 'ajax/badem-ajax-request.php';
} 

/*Load the ajax Request for dashboard full-width fixed heading widget*/
if ( file_exists( BADEM_CORE_INC . 'ajax/badem-ajax-dashboard.php' ) ) {
	require_once BADEM_CORE_INC . 'ajax/badem-ajax-dashboard.php';
}

/*Load the ajax Request for Statistics graph data fetch by database*/
if ( file_exists( BADEM_CORE_INC . 'ajax/badem-ajax-graph.php' ) ) {
	require_once BADEM_CORE_INC . 'ajax/badem-ajax-graph.php';
}

/*Load the ajax Request for Statistics graph data fetch by database*/
if ( file_exists( BADEM_CORE_INC . 'ajax/ajax-handlers.php' ) ) {
	require_once BADEM_CORE_INC . 'ajax/ajax-handlers.php';
}

/*Load the Shortcodes*/
if ( file_exists( BADEM_CORE_INC . 'badem-shortcodes.php' ) ) {
	require_once BADEM_CORE_INC . 'badem-shortcodes.php';
}
/*Load the File Including all function DataBase related.*/
if ( file_exists( BADEM_CORE_INC . 'badem-db-functions.php' ) ) {
	require_once BADEM_CORE_INC . 'badem-db-functions.php';
}

/*Load the File Including all function row data update related.*/
if ( file_exists( BADEM_CORE_INC . 'badem-db-update-functions.php' ) ) {
	require_once BADEM_CORE_INC . 'badem-db-update-functions.php';
}

/*Load the File Including all function row plan data update related.*/
if ( file_exists( BADEM_CORE_INC . 'badem-db-app-plan-functions.php' ) ) {
	require_once BADEM_CORE_INC . 'badem-db-app-plan-functions.php';
}

/*Load the File Including all function row payment data update related.*/
if ( file_exists( BADEM_CORE_INC . 'badem-db-app-payments-functions.php' ) ) {
	require_once BADEM_CORE_INC . 'badem-db-app-payments-functions.php';
}

/*Load the File Including Payment Import SQL data file.*/
if ( file_exists( BADEM_CORE_INC . 'badem-db-core-functions.php' ) ) {
	require_once BADEM_CORE_INC . 'badem-db-core-functions.php';
}

/*Load the File Including Admin pages data*/
if ( file_exists( BADEM_CORE_INC . 'admin/admin-pages.php' ) ) {
	require_once BADEM_CORE_INC . 'admin/admin-pages.php';
}

/*Load the File Including Admin full-width dashboard file*/
if ( file_exists( BADEM_CORE_INC . 'admin/dashboard.php' ) ) {
	require_once BADEM_CORE_INC . 'admin/dashboard.php';
}

/*Load the File Including User Export SQL data file.*/
if ( file_exists( BADEM_CORE_INC . 'admin/data/user_export.php' ) ) {
	require_once BADEM_CORE_INC . 'admin/data/user_export.php';
}

/*Load the File Including User Import SQL data file.*/
if ( file_exists( BADEM_CORE_INC . 'admin/data/user_import.php' ) ) {
	require_once BADEM_CORE_INC . 'admin/data/user_import.php';
}

/*Load the File Including Payment Export SQL data file.*/
if ( file_exists( BADEM_CORE_INC . 'admin/data/payment_export.php' ) ) {
	require_once BADEM_CORE_INC . 'admin/data/payment_export.php';
}

/*Load the File Including Payment Import SQL data file.*/
if ( file_exists( BADEM_CORE_INC . 'admin/data/payment_import.php' ) ) {
	require_once BADEM_CORE_INC . 'admin/data/payment_import.php';
}

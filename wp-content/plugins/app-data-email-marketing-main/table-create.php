<?php
/*
*
* Activation hook to create data tables in database.
*/
class Plugin_table_create {

   	function __construct(){
      register_activation_hook(BADEM_PLUGIN_DIR. 'app-data-email-marketing.php', array($this,'AppaTable_Create'));   
   	}

    /*Activation Function for plugins*/
   	function AppaTable_Create() {
   		update_option('data_access_mode', '0');
      global $wpdb;
      $wpdb->hide_errors();
      require_once(ABSPATH . '/wp-admin/includes/upgrade.php');
      $charset_collate = '';
        if ( $wpdb->has_cap( 'collation' ) ) {
            $charset_collate = $wpdb->get_charset_collate();
        }
      $db_name = $wpdb->prefix .'app_users'; /* New app installation table */
	  $dbp_name = $wpdb->prefix .'badem_platforms'; /* App platforms table */
	  $dba_name = $wpdb->prefix .'badem_apps'; /* All apps list table */
	  $db_plans = $wpdb->prefix .'badem_plans'; /* Apps plans table */
	  $db_paid = $wpdb->prefix.'badem_payments'; /* Apps subscription payment table */
	  $db_apps = $wpdb->prefix.'apps_platform'; /* Apps database table */
      
      	if($wpdb->get_var("show tables like '$db_name'") != $db_name){
         	$sql = "CREATE TABLE $db_name (
				  `id` int(225) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
				  `environment` enum('0','1') DEFAULT NULL,
				  `store_id` varchar(255) NOT NULL,
				  `store_url` varchar(255) NOT NULL,
				  `store_name` varchar(255) NOT NULL,
				  `app_platform` enum('big-commerce','shopify') DEFAULT NULL,
				  `app_id` varchar(255) NOT NULL,
				  `app_name` varchar(255) NOT NULL,
				  `app_status` enum('active','inactive') NOT NULL,
				  `email` varchar(255) NOT NULL,
				  `name` varchar(255) NOT NULL,
				  `address` varchar(255) NOT NULL,
				  `phone` text NOT NULL,
				  `city` varchar(255) NOT NULL,
				  `country` varchar(255) NOT NULL,
				  `zip` text NOT NULL,
				  `current_app_plan` enum('free','paid','trial') NOT NULL,
				  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
				  `payment_status_update_at` timestamp NOT NULL DEFAULT current_timestamp(),
				  `app_status_update_at` timestamp NOT NULL DEFAULT current_timestamp()
				)$charset_collate;";
        	dbDelta($sql);
      	}

      	if($wpdb->get_var("show tables like '$dbp_name'") != $dbp_name){
      		$dbp = "CREATE TABLE $dbp_name (
					  `id` int(225) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
					  `name` varchar(255) NOT NULL,
					  `slug` varchar(255) NOT NULL
					)$charset_collate;";
      		dbDelta($dbp);
      	}
      	if($wpdb->get_var("show tables like '$dba_name'") != $dba_name){
      		$dba = "CREATE TABLE $dba_name (
					  `id` int(225) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
					  `environment` enum('0','1') DEFAULT NULL,
					  `app_id` varchar(255) NOT NULL,
					  `app_name` varchar(255) NOT NULL,
					  `platform` int(11) NOT NULL
					)$charset_collate;";
      		dbDelta($dba);
      	}

      	if($wpdb->get_var("show tables like '$db_paid'") != $db_paid){
			$app_pays = "CREATE TABLE $db_paid (
						  `id` int(225) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
						  `store_id` varchar(255) NOT NULL,
						  `app_id` varchar(255) NOT NULL,
						  `start_at` date NOT NULL,
						  `expired_on` date NOT NULL,
						  `schedule_date` date NOT NULL,
						  `payment_status` enum('paid','failed','in-process') NOT NULL,
						  `transaction_id` varchar(255) NOT NULL,
						  `amount` varchar(255) NOT NULL,
						  `currency` varchar(10) NOT NULL,
						  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
						)$charset_collate;";
			dbDelta($app_pays);
		}

		if($wpdb->get_var("show tables like '$db_apps'") != $db_apps){
			$data_apps = "CREATE TABLE $db_apps (
						  `id` int(225) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
						  `app_name` varchar(255) NOT NULL,
						  `platform` enum('big-commerce','shopify') DEFAULT NULL
						)$charset_collate;";
			dbDelta($data_apps);
		}
	}
}
new Plugin_table_create;


/*if($wpdb->get_var("show tables like '$db_plans'") != $db_plans){
      		$aplans = "CREATE TABLE $db_plans (
					  `id` int(225) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
					  `store_id` varchar(255) NOT NULL,
					  `app_id` varchar(255) NOT NULL,
					  `app_name` varchar(255) NOT NULL,
					  `trial_day` varchar(255) NOT NULL,
					  `plan_id` varchar(255) NOT NULL,
					  `plan_type` enum('recurring','one-time-paid') NOT NULL,
					  `price` varchar(255) NOT NULL,
					  `payment_status` enum('completed','failed','under-process') NOT NULL,
					  `plan_service` enum('free','paid','trial') NOT NULL,
					  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
					)$charset_collate;";
			    dbDelta($aplans);
		}*/

/*****
	Description: Ajax handlers file for handling js code without page refresh.
	Author: BitCot
	Created: 30-Mar-2021
	Modify:

*****/

/*-----------------------------------------------------
* Bitcot Apps Data & Email Marketing Plugin JS / jQuery
* Admin page Jquery DataTables script
------------------------------------------------------*/

var imgHide = '.img-hide';
var app_users_list = '#app_users_list';
var searchByPlatform = '#searchByPlatform';
var searchBystatus = '#searchBystatus';
var searchByAppsPlans = '#searchByAppsPlans';
var pick_from_date = '#from_app_date_picker';
var pick_to_date = '#to_app_date_picker';
var plugin_img_url = badem_object.plugin_url;

( function( $ ) {

	$(document).ready(function() {
		
		if( $( app_users_list ).length ){
			$dis_list_all = $( app_users_list ).DataTable({
				processing: true,
				serverSide: true,
			    ajax: {
			      dataSrc:"data",
			      data:function(data){
			      	var pname = $(searchByPlatform).val();
          			data.fltr_platform = pname;
          			if( $( searchByApps ).length ){
          				data.fltr_app = $(searchByApps).val();
          				data.fltr_app_status = $(searchBystatus).val();
          				data.fltr_app_type = $(searchByAppsPlans).val();
          				data.fltr_from_date = $(pick_from_date).val();
          				data.fltr_to_date = $(pick_to_date).val();
          			}
			      },
			      url: ajaxurl + '?action=get_appusers_list_for_datatables',
			      dataFilter: function(data){
			      		$(imgHide).hide();
						/*console.log(data);*/
			            var json = JSON.parse( data );
			            json.recordsTotal = json.total;
			            json.recordsFiltered = json.total;
			            json.data = json.data;
			            $( searchByPlatform ).empty().html( json.platforms );
			            $( searchByApps ).empty().html( json.filter_app );
			            /*console.log(json);*/
			            return JSON.stringify( json ); /*return JSON string*/
			        }
			    },
			    columns: [
			        { data: 'id' },
			        { data: 'store_id' },
			        { data: 'app_id' },
			        { data: 'store_name' },
			        { data: 'app_name' },
			        { data: 'name' },
			        { data: 'email' },
			        { data: 'app_status' },
			        { data: 'created_at' },
			        { data: 'current_app_plan' },
			        { data: 'mail_lists' }
			    ],
			    "aoColumnDefs": [
			        { "bSortable": false, "aTargets": [ 7,9,10 ] }, 
			        { "bSearchable": false, "aTargets": [ 0,6,7,8,9 ] }
			    ],
			    order: [[ 0, "desc" ]],
			    /*"language": { loadingRecords: '&nbsp;', processing: '<img src="'+plugin_img_url+'transparent-loading.gif" /><span class="sr-only">Loading...</span>'},*/
			});

			if( $( searchByPlatform ).length ){
				$( document ).on( 'change', searchByPlatform, searchByPlatformCallback );
			}
			if( $( searchByApps ).length ){
				$( document ).on( 'change', searchByApps, searchByAppsCallback );
			}
			if($(searchBystatus).length){
				var status_text = $( searchBystatus ).val();
				if( status_text == 'active' )
				{
					$( created_at ).text('Install date');
				}
				
				$( document ).on( 'change', searchBystatus, searchByAppsCallback );	
			}
			if($(searchByAppsPlans).length){
				$( document ).on( 'change', searchByAppsPlans, searchByAppsCallback );	
			}
			if($(pick_from_date).length){
				$( document ).on( 'change', pick_from_date, searchByAppsCallback );
			}
			if($(pick_to_date).length){
				$( document ).on( 'change', pick_to_date, searchByAppsCallback );
			}
			function searchByPlatformCallback(e){
				$(imgHide).show();
				update_apps_dropdown_optoins(this.value);
				/*$dis_list_all.draw();*/
			}
			function searchByAppsCallback(e){
				var status_text = $( searchBystatus ).val();
				if( status_text == 'active' )
				{
					$( created_at ).text('Install date');
				}else{
					$( created_at ).text('Uninstall date');
				}

				$(imgHide).show();
				$dis_list_all.draw();
			}
			function update_apps_dropdown_optoins(v){
				$.get(ajaxurl+'?action=get_appnames_list_for_datatables&pf='+v, function(data){
					console.log(data);
					if(data){
						$(searchByApps).empty().append( $(data) );
						$dis_list_all.draw();
					}
				} );
			}

		}
	} );

	/* Modal popup on show data append by ajax */
	$('#myModal').on('show.bs.modal', function(){
		$('#myModal').find('.modal-body').html('');
		var id = jQuery('input[name="app_user_id"]').val();
		$('#myModal').find('.modal-body').html('<div class="text-center"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span></div>');
	    $.get(ajaxurl +'?action=badem_custom_plugin_frontend_ajax&id=' + id, function(data){
	        $('#myModal').find('.modal-body').html(data);
	    })
	});

} )( jQuery );


/* Modal popup onclick function callback */
function show_quick_user_view(id){
	jQuery('input[name="app_user_id"]').val(id);
	jQuery('#myModal').modal({ show : true });
}
/*-----------------------------------------------------
* Bitcot Apps Data & Email Marketing Plugin JS / jQuery
* Export data Jquery DataTables script
------------------------------------------------------*/
var exp_hinput = "#exp_hinput";
var exp_query_click = "#exp_query_click";
var paid_export = "#paid_export";
var ptb_hquery = "#ptb_hquery";

var searchBystatus = "#searchBystatus";
var searchByPlatform = "#searchByPlatform";
var searchByApps = "#searchByApps";
var searchByAppsPlans = "#searchByAppsPlans";
var from_app_date_picker = "#from_app_date_picker";
var to_app_date_picker = "#to_app_date_picker";

var app_plaform = "#app_plaform";
var apps_info = "#apps_info";
var payment_status = "#payment_status";
var from_pay_date_picker = "#from_pay_date_picker";
var to_pay_date_picker = "#to_pay_date_picker";

var field = 'input:radio[name="field"]';
var option = ".field_style";
var usr_id = "#usr_id";
var str_id = "#str_id";
var ap_name = "#ap_name";
var usr = "#usr";
var mail = "#mail";

( function( $ ) {

	$(document).ready(function() {
		var exp_id='', exp_sid='', exp_appname='', exp_user='', exp_email='';
		var radio = $( field ).val();
			$( option ).hide();
            
            $( field ).change(function() {
                
                if( $(this).val() == 'field' )
                {
                	radio = $(this).val(); 
                    $( option ).show();    
                }else{
                    radio = $(this).val();
                    exp_id='';
                    exp_sid='';
                    exp_appname='';
                    exp_user='';
                    exp_email='';
                    $( option ).hide();
                }
                
            } );

            $( usr_id ).click( function(){
				exp_id = $( usr_id ).val();	
			});
			$( str_id ).click( function(){
				exp_sid = $( str_id ).val();
			});
			$( ap_name ).click( function(){
				exp_appname = $( ap_name ).val();
			});
			$( usr ).click( function(){
				exp_user = $( usr ).val();
			});
			$( mail ).click( function(){
				exp_email = $( mail ).val();
			});

		$( 'body' ).on( 'click', exp_query_click, function(e){
			e.preventDefault();
			var status = $(searchBystatus).val();
			var platform = $(searchByPlatform).val();
			var apps = $(searchByApps).val();
			var plans = $(searchByAppsPlans).val();
			var ufrm = $(from_app_date_picker).val();
			var uto = $(to_app_date_picker).val();

			$( usr_id ).prop('checked', false);
			$( str_id ).prop('checked', false);
			$( ap_name ).prop('checked', false);
			$( usr ).prop('checked', false);
			$( mail ).prop('checked', false);
			
			var a_link = $(this).attr('href');

			if( exp_id || exp_sid || exp_appname || exp_user || exp_email || radio == 'all')
			{
				window.location.href = a_link+'&status='+status+'&plform='+platform+'&apps='+apps+'&plans='+plans+'&ufrm='+ufrm+'&uto='+uto+'&rad='+radio+'&id='+exp_id+'&sid='+exp_sid+'&app='+exp_appname+'&usr='+exp_user+'&email='+exp_email;
			}else
			{
				alert('Please tick at least one option.');
			}
			
		} );


		$( 'body' ).on( 'click', paid_export, function(e){
			e.preventDefault();
			var platforms = $(app_plaform).val();
			var appsinfo = $(apps_info).val();
			var pystatus = $(payment_status).val();
			var payfrm = $(from_pay_date_picker).val();
			var payto = $(to_pay_date_picker).val();

			var g_link = $(this).attr('href');

			window.location.href = g_link+'&pform='+platforms+'&appsinfo='+appsinfo+'&pystatus='+pystatus+'&payfrm='+payfrm+'&payto='+payto;
		} );

	});

} )( jQuery );

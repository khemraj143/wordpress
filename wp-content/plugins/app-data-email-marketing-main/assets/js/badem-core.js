
/*****
	Description: Ajax handlers file for handling js code without page refresh.
	Author: BitCot
	Created: 30-Mar-2021
	Modify: 31-Mar-2021

*****/

/*-----------------------------------------------------
* Bitcot Apps Data & Email Marketing Plugin JS / jQuery
------------------------------------------------------*/
	/***** For Dahboard page *****/
	var pltform = '#plaform_apps';
	var imgHide = '.img-hide';
	var slcApps = '#select_apps';
	var recent = '#recent_row';
	var install_total_app = '#install_total_app';
	var install_year_app = '#install_year_app';
	var install_month_app = '#install_month_app';
	var uninstall_total_app = '#uninstall_total_app';
	var uninstall_year_app = '#uninstall_year_app';
	var uninstall_month_app = '#uninstall_month_app';
	var reven_month = '#reven_month';
	var reven_year = '#reven_year';
	var reven_total = '#reven_total';
	var from_dash_date_picker = '#from_dash_date_picker';
	var to_dash_date_picker = '#to_dash_date_picker';
	var date_picker = '.date_picker_input';	

	/***** For Payment History page *****/
	var app_platform = "#app_plaform";
	var Pay_status = "#payment_status";
	var pay_from = "#from_pay_date_picker";
	var pay_to = "#to_pay_date_picker";
	var pay_data_table = "#pay_data_table";
	var apps_info = "#apps_info";
	var plugin_img_url = badem_object.plugin_url;

jQuery(document).ready(function($){

	if( pagenow == 'toplevel_page_app_users_list' ||
		pagenow == 'dashboard' ||
		pagenow == 'app-users_page_payments_info' ||
		pagenow == 'app-users_page_statistics_graph' )
	{
			$(date_picker).datepicker({
			    dateFormat: "yy-mm-dd",
			    maxDate: 0
				});
	}

	$('.nav-item .nav-link').click(function(){
		var impdiv = $(this).attr('data-tbl');
		if(impdiv == 'import_table'){
			$('#export_table').hide();
			$('#import_table').show();
		}
		if(impdiv == 'export_table'){
			$('#import_table').hide();
			$('#export_table').show();
		}
		$('.nav-link').removeClass(' active');
	    $(this).addClass('active');
	});

	if(pagenow == 'app-users_page_access_data_mode' ||
		pagenow == 'toplevel_page_app_users_list' ||
		pagenow == 'app-users_page_payments_info'){
		setTimeout(function(){
			   	$('.notice-success').hide();
			}, 3000);
		/*setTimeout(function(){
			   	$('.notice-error').hide();
			}, 3000);*/
	}

		if( pagenow == 'dashboard' ){

			showappdatabyajax();

			$('body').on('change', pltform, function(e){
				$(imgHide).show();
				showappdatabyajax();				
			});

			$('body').on('change', slcApps, function(e){
				$(imgHide).show();
				showappdatabyajax();				
			});

			$('body').on('change', to_dash_date_picker, function(e){
				$(imgHide).show();
				showappdatabyajax();				
			});

			$('body').on('change', from_dash_date_picker, function(e){
				$(imgHide).show();
				showappdatabyajax();				
			});

			function showappdatabyajax(){
				var fltr_platform = $(pltform).val();
				var fltr_app = $(slcApps).val();
				var fltr_from_date = $(from_dash_date_picker).val();
				var fltr_to_date = $(to_dash_date_picker).val();

				var ap_data = {
					'action': 'platforms',
					'fltr_platform': fltr_platform,
					'fltr_app': fltr_app,
					'fltr_from_date' : fltr_from_date,
					'fltr_to_date' : fltr_to_date,
					'revenue' : 'paid',
					'start' : 0,
					'length' : 10
				};
				jQuery.post(ajaxurl, ap_data, function(res){
					$(imgHide).hide();

					var data = JSON.parse(res);
					/*console.log(data);*/
					
					if( data.apps_total.act_month.results[0] ){
						$(install_month_app).html(data.apps_total.act_month.results[0].month);
					}
					if( data.apps_total.act_month.results[0] ){
						$(uninstall_month_app).html(data.apps_total.act_month.results[0].month);
					}
					if( data.apps_total.act_year.results[0] ){
						$(install_year_app).html(data.apps_total.act_year.results[0].year);
					}
					if( data.apps_total.in_year.results[0] ){
						$(uninstall_year_app).html(data.apps_total.in_year.results[0].year);
					}
					if( data.apps_total.act_total.results[0] ){
						$(install_total_app).html(data.apps_total.act_total.results[0].total);
					}
					if( data.apps_total.in_total.results[0] ){
						$(uninstall_total_app).html(data.apps_total.in_total.results[0].total);
					}

					if( data.total_revenue.month.results[0].month == null ){
						$(reven_month).html('0');
					}else{
						$(reven_month).html(data.total_revenue.month.results[0].month);
					}

					if( data.total_revenue.year.results[0].year == null ){
						$(reven_year).html('0');
					}else{
						$(reven_year).html(data.total_revenue.year.results[0].year);
					}

					if( data.total_revenue.total.results[0].total == null ){
						$(reven_total).html('0');
					}else{
						$(reven_total).html(data.total_revenue.total.results[0].total);
					}
					/**/
					if( data.tables )
					{
						$( recent ).html( data.tables );
					}

					if( data.app_list )
					{
						$( slcApps ).html( data.app_list );
					}

					if( data.show_pform )
					{
						console.log(data.show_pform);
						$( pltform ).html( data.show_pform );	
					}
					
					/*console.log(res);*/
				});
			}
		}
		
		/**** For payments page ****/
		if( pagenow == 'app-users_page_payments_info'){
			/*$(imgHide).hide();*/
			if( $( pay_data_table ).length ){
				$dis_list_all = $( pay_data_table ).DataTable({
					processing: true,
					serverSide: true,
				    ajax: {
				      dataSrc:"data",
				      data:function(data){
				      	data.fltr_platform = $(app_platform).val();
	      				data.fltr_app = $(apps_info).val();
	      				data.fltr_pay_status = $(Pay_status).val();
	      				data.fltr_from_date = $(pay_from).val();
	      				data.fltr_to_date = $(pay_to).val();
				      },
				      url: ajaxurl + '?action=badem_paid_information',
				      dataFilter: function(data){
				      		$(imgHide).hide();
							/*console.log(data);*/
				            var json = JSON.parse( data );
				            json.recordsTotal = json.total;
				            json.recordsFiltered = json.total;
				            json.data = json.data;
				            $(apps_info).empty().html(json.apps);
				            
				            return JSON.stringify( json ); /*return JSON string*/
				        }
				    },
				    columns: [
				        { data: 'app_id' },
				        { data: 'app_name' },
				        { data: 'name' },
				        { data: 'email' },
				        { data: 'start_at' },
				        { data: 'expired_on' },
				        { data: 'payment_status' },
				        { data: 'amount' },
				        { data: 'schedule_date' },

				    ],
				    "aoColumnDefs": [
				        { "bSortable": false, "aTargets": [ 0,7 ] }, 
				        { "bSearchable": false, "aTargets": [ 0,1,5,6,7,8 ] }
				    ],
				    order: [[ 0, "desc" ]],
				    /*"language": { loadingRecords: '&nbsp;', processing: '<img src="'+plugin_img_url+'transparent-loading.gif" /><span class="sr-only">Loading...</span>'},*/
				});
				if($(app_platform).length){
					$('body').on('change', app_platform, dataLoadcallBack );
				}

				if($(apps_info).length){
					$('body').on('change', apps_info, dataLoadcallBack );
				}
				
				if($(Pay_status).length){
					$('body').on('change', Pay_status, dataLoadcallBack );
				}

				if($(pay_from).length){
					$('body').on('change', pay_from, dataLoadcallBack );
				}

				if($(pay_to).length){
					$('body').on('change', pay_to, dataLoadcallBack );
				}

				function dataLoadcallBack(e){
					$(imgHide).show();
					$dis_list_all.draw();
				}
			}
		}
});

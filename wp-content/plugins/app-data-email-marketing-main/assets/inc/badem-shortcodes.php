<?php 
/*
*
*	***** Bitcot Apps Data & Email Marketing *****
*
*	Shortcodes
*	
*/
/*If this file is called directly, abort.*/ 
if ( ! defined( 'WPINC' ) ) {die;} // end if
/*
*
*  Build The Custom Plugin Form
*
*  Display Anywhere Using Shortcode: [badem_custom_plugin_form]
*
*/
function badem_custom_plugin_form_display($atts, $content = NULL){
		extract(shortcode_atts(array(
      	'el_class' => '',
      	'el_id' => '',	
		),$atts));    
        
        $out ='';
        $out .= '<div id="badem_custom_plugin_form_wrap" class="badem-form-wrap">';
        $out .= 'Hey! Im a cool new plugin named <strong>Child Page Auto Add!</strong>';
        $out .= '<form id="badem_custom_plugin_form">';
        $out .= '<p><input type="text" name="myInputField" id="myInputField" placeholder="Test Field: Test Ajax Responses"></p>';
        
        // Final Submit Button
        $out .= '<p><input type="submit" id="submit_btn" value="Submit My Form"></p>';        
        $out .= '</form>';
         // Form Ends
        $out .='</div><!-- badem_custom_plugin_form_wrap -->';       
        return $out;
}
/*
Register All Shorcodes At Once
*/
add_action( 'init', 'badem_register_shortcodes');
function badem_register_shortcodes(){
  /*Registered Shortcodes*/
  add_shortcode ('badem_custom_plugin_form', 'badem_custom_plugin_form_display' );
};
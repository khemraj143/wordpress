<?php

/*****
	Description: Apps manager page for managing apps by admin.
	Author: BitCot
	Created: 24-Mar-2021
	Modify:

*****/

?>

<div class="wrap setting_mode">
	<h1> Apps manager page </h1>
			<div class="img-hide">
				<img src="<?php echo BADEM_CORE_IMG; ?>transparent-loading.gif" alt="Loader-gif">
			</div>

		<a id="add_form" class="btn btn-primary float-right" data-toggle="modal" data-target="#form_display" href=""> Add new
	  	</a>

		<table id="apps_table" class="table table-hover dt-responsive nowrap">
			<thead>
	            <tr>
	            	<th> App id </th>
	            	<th> Apps </th>
	            	<th> Platforms </th>
	            	<th> Options </th>
	            </tr>
        	</thead>
        	<tbody>
        		
        	</tbody>
		</table>

		<!-- The Modal -->
		<div class="modal fade" id="form_display" data-keyboard="false" data-backdrop="static">
		    <div class="modal-dialog modal-dialog-centered modal-lg">
		      	<div class="modal-content">
		      
		        <!-- Modal Header -->
		        <div class="modal-header">
		        	<h4 class="modal-title">Modal Heading</h4>
		        </div>
		        
		        <!-- Modal body -->
		        <div class="row modal-body">
		         	<div class="container d-grid gap-3">
						<form class="manage_apps_style" id="add_apps" action="<?php echo create_nonce_at("admin.php?page=apps_manage_page", "add_apps", "apps_action", "new_apps"); ?>" method="POST"> 
							<div class="row">
								<div class="col-sm-5">
									<div class="form-group">
								      	<input type="text" id="apps_name" class="form-control" placeholder="Write app name here." name="app_name" required>
								    </div>	
								</div>
								<div class="col-sm-5">
									<select class="form-control" id="platform" name="app_platform" required>
										<option value=""> Select Platform </option>
										<option value="big-commerce"> BigCommerce </option>
										<option value="shopify"> Shopify </option>
									</select>
								</div>
								<div class="col-sm-2">
										<input type="hidden" id="app_id" name="app_id" value="">
										<button id="form_btn" type="submit" class="btn btn-primary" name="add" value="add"> Save </button>
								</div>
							</div>
						    
						</form>
					</div>
		        </div>
		        
		        <!-- Modal footer -->
		        <div class="modal-footer">
		        	<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		        </div>
		        
		      	</div>
		    </div>
		</div>

</div>

<script type="text/javascript">

	var apps_table = "#apps_table";
	var add_form = "#add_form";
	var form_btn = "#form_btn";
	var data_id = "#row_data_id";
	var edit_apps = "#edit_apps";
	var update_name = "#apps_name";
	var update_platform = "#platform";
	var app_id = "#app_id";
	var collapse = ".collapse";
	var close_btn = "#close-btn";
	var modal_title = ".modal-title";
	var imgHide = '.img-hide';

	( function ($) 
		{
			$(document).ready( function()

			{
				$( apps_table ).DataTable(
				{
					processing: true,
					serverSide: true,
				    ajax:
				    {
				      dataSrc:"data",
				      url: ajaxurl + '?action=get_apps_data_table',
				      dataFilter: function(apps_data)
				      {
				      		$(imgHide).hide();
				            var json = JSON.parse( apps_data );
				            json.recordsTotal = json.total;
				            json.recordsFiltered = json.total;
				            json.data = json.data;
				            console.log(json);
				            return JSON.stringify( json ); /*return JSON string*/
				        }
				    },
				    columns: [
				        { data: 'id' },
				        { data: 'app_name' },
				        { data: 'platform' },
				        { data: 'action' }
				    ],
				    "aoColumnDefs": [
				        { "bSortable": false, "aTargets": [ 3 ] }, 
				        { "bSearchable": false, "aTargets": [ 0,3 ] }
				    ]
				} );


				$('body').on( 'click', add_form , function()
				{
					$(form_btn).text('Save');
					$(modal_title).text('Add new app');
					$(close_btn).hide();
					$(app_id).empty().val('');
					$(update_name).empty().val('');
					$(update_platform).val('');
				} );

				/** Edit button modal form **/
				$('body').on( 'click', data_id, function(e) 
					{
						e.preventDefault();
						var row_data = $( this ).attr('data-row');

						jQuery.post(ajaxurl,
							data =
							{
								action: 'edit_app_data',
								data_id: row_data  
							},
							function(data_result)
							{
								var data = JSON.parse(data_result);
								$(form_btn).text('Update');
								$(modal_title).text('Update App');
								$(close_btn).show();
								$(app_id).val(data.apps.id);
								$(update_name).val(data.apps.app_name);
								$(update_platform).val(data.apps.platform);
								$( '#form_display' ).modal('show');
							}

							);

					} );

			} );

		} )( jQuery );
</script>
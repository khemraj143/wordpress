<?php

/*****
	Description: Ajax handlers file for handling js code without page refresh.
	Author: BitCot
	Created: 30-Mar-2021
	Modify: 01-Apr-2021

*****/

add_action( 'admin_footer', 'badem_custom_dashboard_widget' );
function badem_custom_dashboard_widget() {
	/* Bail if not viewing the main dashboard page */
	if ( get_current_screen()->base !== 'dashboard' ) {
		return;
	}
	wp_enqueue_style( 'badem-bootstrap-min' );
	wp_enqueue_style( 'badem-jquery-ui-css' );
	wp_enqueue_script( 'badem-bootstrap-min' );
	wp_enqueue_script( 'jquery-ui-datepicker' );
	?>
	
	<div id="custom-id" class="welcome-panel dashboard_warp setting_mode" style="display: none;">
		<div class="custome_widget_content">
			<!-- Loader div -->
			<div class="img-hide">
				<img src="<?php echo BADEM_CORE_IMG; ?>transparent-loading.gif" alt="Loader-gif">
			</div>
			<!-- Title -->
			<h2 class="h2_title center_cont"> Recent Activity </h2>
			<!-- platform data table -->

			<table class="table table-bordered dashboard-table">
				<thead class="text-center">
					<tr>
						<th colspan="2"> BigCommerce </th>
						<th colspan="2"> Shopify </th>
					</tr>
					<tr>
						<th> Install </th>
						<th> Uninstall </th>
						<th> Install </th>
						<th> Unistall </th>
					</tr>
				</thead>
				<tbody id="recent_row">
				</tbody>
			</table>

			<div class="row dashrow_filter">
				<div class="col-md-2 col-sm-4">
					<select id="plaform_apps" name="apps_name">
						
					</select>
				</div>
				<div class="col-md-2 col-sm-4">
					<select id="select_apps" name="sub_apps">
				
					</select>
				</div>
				<div class="col-md-2 col-sm-4">
					<div class="date-picker"> 
						<input type="text" name="" class="date_picker_input" placeholder="From date." id="from_dash_date_picker" /> 
					</div>
				</div>
				<div class="col-md-2 col-sm-4">
					<div class="date-picker"> 
						<input type="text" name="" class="date_picker_input" placeholder="To date." id="to_dash_date_picker" /> 
					</div>
				</div>
			</div>
			
			<section class="revenue_data">
	            <h2 class="h2_title">Revenue Table</h2>
	            <div class="row">
	                <div class="col-md-4">
	                    <div class="card_panel pricingTable">
	                    	  <div class="icon_circle">
				      			<img src="<?php echo BADEM_CORE_IMG; ?>Monthly.png" alt="Monthly revanue" />
				              </div>
	                        <h4 class="card_title">Monthly Revenue</h4>
	                        <div class="price-value">
	                            <!-- <span class="currency">$</span> -->
	                        	$<span id="reven_month">0</span>
	                            <span class="month">/Month</span>
	                        </div>        
	                    </div>
	                </div>

	                <div class="col-md-4">
	                    <div class="card_panel pricingTable">
	                    	 <div class="icon_circle">
				      			<img src="<?php echo BADEM_CORE_IMG; ?>Yearly.png" alt="Yearly revanue" />
				             </div>
	                        <h4 class="card_title">Yearly Revenue</h4>
	                        <div class="price-value">
	                            <!-- <span class="currency">$</span> -->
	                        	$<span id="reven_year">0</span>
	                            <span class="month">/Year</span>
	                        </div>
	                    </div>
	                </div>

	                <div class="col-md-4">
	                    <div class="card_panel pricingTable">
	                    	<div class="icon_circle">
				      			<img src="<?php echo BADEM_CORE_IMG; ?>Total.png" alt="Total revanue" />
				            </div>
	                        <h4 class="card_title">Total Revenue</h4>
	                        <div class="price-value">
	                        	<!-- <span class="currency">$</span> -->
	                        	$<span id="reven_total">0</span>
	                            <span class="month"> Total</span>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </section>

			<!-- START Revenue data table display by dropdown -->
			<section class="apps_data">
			<h2 class="h2_title">Apps Summary</h2>
				<div class="row">
					<div class="col-lg-4 col-md-4">
					    <div class="card_panel revenue_card ins_monthly_revanue">
					      	<h4 class="card_title">Monthly Installed Apps</h4>
					      	<div class="icon_circle">
					      		<img src="<?php echo BADEM_CORE_IMG; ?>revenue_monthly.png" alt="Monthly revanue" />
					      	</div>				       
					        <h4 id="install_month_app" class="h4_title fontbold"></h4>
					      	<h5 class="h5_title txt_sucess">Installed App</h5>
					    </div>
					</div>
					<div class="col-lg-4 col-md-4">
					    <div class="card_panel revenue_card ins_yearly_revanue">
						    <h4 class="card_title">Yearly Installed Apps</h4>
						    <div class="icon_circle">
						       <img src="<?php echo BADEM_CORE_IMG; ?>revenue_total.png" alt="Monthly revanue" />
						   	</div>
						    <h4 id="install_year_app" class="h4_title fontbold"></h4>
						    <h5 class="h5_title txt_sucess">Installed App</h5>
					    </div>
					</div>
					<div class="col-lg-4 col-md-4">
					    <div class="card_panel revenue_card ins_yearly_revanue">
					      	<h4 class="card_title">Total Installed Apps</h4>
					      	<div class="icon_circle">
					       		<img src="<?php echo BADEM_CORE_IMG; ?>revenue_yearly.png" alt="Monthly revanue" />
					   		</div>
					      	<h4 id="install_total_app" class="h4_title fontbold"></h4>
					      	<h5 class="h5_title txt_sucess">Installed App</h5>
					    </div>
					</div>		
				</div>
				<div class="row">
					<div class="col-lg-4 col-md-4">
					    <div class="card_panel revenue_card ins_yearly_revanue">
	  						<h4 class="card_title">Monthly Uninstalled Apps</h4>
	  						<div class="icon_circle">
					       		<img src="<?php echo BADEM_CORE_IMG; ?>uninstall.png" alt="Monthly revanue" />
					   		</div>
					      	<h4 id="uninstall_month_app" class="h4_title fontbold"></h4>
					      	<h5 class="h5_title txt_danger">Uninstalled App</h5>
					    </div>
					</div>
					<div class="col-lg-4 col-md-4">
						<div class="uni__yearly_revanue">
					    	<div class="card_panel revenue_card">
						     	<h4 class="card_title">Yearly Uninstalled Apps</h4>
							    <div class="icon_circle">
							      <img src="<?php echo BADEM_CORE_IMG; ?>uninstall.png" alt="Monthly revanue" />
							  	</div>
						      	<h4 id="uninstall_year_app" class="h4_title fontbold"></h4>
						      	<h5 class="h5_title txt_danger">Uninstalled App</h5>
					    	</div>
						</div>
					</div>
					<div class="col-lg-4 col-md-4">
					    <div class="card_panel revenue_card ins_yearly_revanue">
					    	<h4 class="card_title">Total Uninstalled Apps</h4>
					    	<div class="icon_circle">
					    	 	<img src="<?php echo BADEM_CORE_IMG; ?>uninstall.png" alt="Monthly revanue" />				     
					    	</div>
					      	<h4 id="uninstall_total_app" class="h4_title fontbold"></h4>
					      	<h5 class="h5_title txt_danger">Uninstalled App</h5>
					    </div>
					</div>
				</div>
			</section>
			<!-- END Revenue data table display by dropdown -->
		</div>
	</div>

	<script>
		jQuery(document).ready(function($) {
			$('#welcome-panel').after($('#custom-id').show());
		});
	</script>
	<?php 
}
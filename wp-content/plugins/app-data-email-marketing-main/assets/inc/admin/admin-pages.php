<?php
/*
*
*	***** Bitcot Apps Data & Email Marketing *****
*
*	Admin Pages
*	
*/
/*If this file is called directly, abort.*/
if ( ! defined( 'WPINC' ) ) {die;}  /*end if*/
/*
*
* Admin custom menu and pages
*
*/

class BADEM_Admin_Pages
{
	
	public function __construct()
	{
		add_action( 'admin_menu', array( $this, 'badem_admin_menu_page' ) );
	}

	public function badem_admin_menu_page()
	{
		$menu = add_menu_page( 
	        __( 'App Users' ),
	        'App Users',
	        'manage_options',
	        'app_users_list',
	        array( $this, 'badem_admin_menu_page_callback' ),
	        'dashicons-paperclip',
	        7
	    );

	    $submenu_1 = add_submenu_page( 'app_users_list', 'App Users', 'App Users', 'manage_options', 'app_users_list' );

	    $submenu_2 = add_submenu_page( 'app_users_list', 'Payment History', 'Payments', 'manage_options', 'payments_info', array($this,'badem_sub_menu_payment_page_callback') );

	    $submenu_3 = add_submenu_page( 'app_users_list', 'Data open Mode', 'Mode setting', 'manage_options', 'access_data_mode', array($this,'badem_sub_menu_page_callback') );

	    $submenu_4 = add_submenu_page( 'app_users_list', 'Apps Data', 'Apps', 'manage_options', 'apps_manage_page', array($this,'badem_apps_manage_page_callback') );

	    $submenu_5 = add_submenu_page( 'app_users_list', 'Statistics Graph', 'Graph', 'manage_options', 'statistics_graph', array($this,'badem_statistics_page_callback') );



	    add_action( 'admin_print_styles-' . $menu, array( $this, 'badem_admin_page_styles' ) );
		add_action( 'admin_print_scripts-' . $menu, array( $this, 'badem_admin_page_scripts' ) );
	}

	public function badem_admin_page_styles()
	{
		wp_enqueue_style('badem_adminpage_style', BADEM_CORE_CSS . 'badem_adminpage.css', array(), time(), 'all' );

		wp_enqueue_style( 'badem-dataTables' );
		wp_enqueue_style( 'badem-dataTables-responsive' );
		wp_enqueue_style( 'badem-dataTables-rowReorder' );	
		wp_enqueue_style( 'badem-bootstrap-min' );
		wp_enqueue_style( 'badem-font-awesome' );
		wp_enqueue_style( 'badem-jquery-ui-css' );
	}

	public function badem_admin_page_scripts()
	{
		wp_enqueue_script('jquery-ui-datepicker');
		wp_enqueue_script( 'badem_adminpage-script', BADEM_CORE_JS . 'badem_adminpage.js', array( 'jquery', 'badem-popper', 'badem-bootstrap-min' ), time(), true );
	}

	public function badem_admin_menu_page_callback()
	{
		wp_enqueue_script('badem-datatables');
		wp_enqueue_script('badem-datatables-roworder');
		wp_enqueue_script('badem-datatables-responsive');
		wp_enqueue_script('badem-popper');
		wp_enqueue_script('badem-bootstrap-min');
		wp_enqueue_script( 'jquery-ui-datepicker' );
		wp_enqueue_script( 'export_query_min' );
		
		require_once( BADEM_CORE_INC . 'admin/admin-pages/app_users_main.php' );
	}


	public function badem_sub_menu_page_callback()
	{
		require_once( BADEM_CORE_INC . 'admin/admin-pages/mode_setting.php' );
	}

	public function badem_sub_menu_payment_page_callback()
	{
		wp_enqueue_style( 'badem-dataTables' );
		wp_enqueue_style( 'badem-dataTables-responsive' );
		wp_enqueue_style( 'badem-dataTables-rowReorder' );
		wp_enqueue_style( 'badem-bootstrap-min' );
		wp_enqueue_style( 'badem-jquery-ui-css' );
		wp_enqueue_script('badem-datatables');
		wp_enqueue_script('badem-datatables-roworder');
		wp_enqueue_script('badem-datatables-responsive');
		wp_enqueue_script( 'badem-bootstrap-min' );
		wp_enqueue_script( 'jquery-ui-datepicker' );
		wp_enqueue_script( 'export_query_min' );
		require_once( BADEM_CORE_INC . 'admin/admin-pages/admin_payment_manage.php' );	
	}
	
	
	public function badem_apps_manage_page_callback()
	{
		wp_enqueue_style( 'badem-dataTables' );
		wp_enqueue_style( 'badem-dataTables-responsive' );
		wp_enqueue_style( 'badem-dataTables-rowReorder' );
		wp_enqueue_style( 'badem-bootstrap-min' );
		wp_enqueue_style( 'badem-jquery-ui-css' );
		wp_enqueue_script('badem-datatables');
		wp_enqueue_script('badem-datatables-roworder');
		wp_enqueue_script('badem-datatables-responsive');
		wp_enqueue_script( 'badem-bootstrap-min' );
		require_once( BADEM_CORE_INC . 'admin/manage_apps.php' );
	}


	public function badem_statistics_page_callback()
	{
		wp_enqueue_style( 'badem-bootstrap-min' );
		wp_enqueue_style( 'badem-jquery-ui-css' );
		wp_enqueue_script( 'badem-bootstrap-min' );
		wp_enqueue_script( 'jquery-ui-datepicker' );
		wp_enqueue_script( 'jquery-jqplot-js' );
		wp_enqueue_script( 'jqplot-highlighter-js' );
		wp_enqueue_script( 'jqplot-cursor-js' );
		wp_enqueue_script( 'jqplot-dateAxisRenderer-js' );
		wp_enqueue_script( 'jqplot-pieRenderer-js' );
		
		require_once( BADEM_CORE_INC . 'admin/statistics_graph.php' );	
	}

}

new BADEM_Admin_Pages;
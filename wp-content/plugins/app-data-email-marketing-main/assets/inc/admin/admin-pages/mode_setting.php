<?php
wp_enqueue_style( 'badem-bootstrap-min' );
if(isset($_POST['sub_button'])){
	$option_name = 'data_access_mode';
	$new_value =  $_POST['data_mode'];
	$sett_update = update_option($option_name, $new_value);
	if ( $sett_update ) {
        ?>
 
            <div class="notice notice-success is-dismissible">
                <p><?php _e('Setting has been save successfully', 'textdomain') ?></p>
            </div>
 
    <?php }
}

?>

<div class="wrap setting_mode">
	<form method="POST">
	<h1>Data choosen by mode</h1>	
		<div class="card cst_card">			
			<div class="card-body">
				<h5 class="h5_title">Select data accessing mode</h5>
				<div class="form-field">
					 <input type="radio" id="data_mode" name="data_mode" value='0' <?php if(get_option( 'data_access_mode' ) == '0') echo "checked"; ?>>
					<label for="sandbox"> Sandbox mode</label>&nbsp; 
					<input type="radio" id="data_mode" name="data_mode" value='1' <?php if(get_option( 'data_access_mode' ) == '1') echo "checked"; ?>>
					<label for="live"> Live mode</label>
				</div>								
			</div>		
		</div>
		<div class="btn_blc">
		  <button type="submit" class="btn btn_primary" name="sub_button">Save</button>
	    </div>
	    </form>	
</div>

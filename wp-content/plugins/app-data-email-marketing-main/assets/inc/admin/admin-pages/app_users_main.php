<?php

/*****
    Description: Ajax handlers file for handling js code without page refresh.
    Author: BitCot
    Created: 30-Mar-2021
    Modify: 01-Apr-2021

*****/

/*<option value=''>All App Platforms</option> 
                if( $badem_platforms ){
                    foreach ($badem_platforms as $key => $value) {
                        echo '<option value='.$key.'>'.$value.'</option>';
                    }
                }
global $badem_platforms;*/

function generate_nonce_url($tact){
    if($tact){
        $exp_url = admin_url().'admin.php?page=app_users_list';
        $exp_data = add_query_arg( 
                        array( 
                            'action' => $tact,

                        ), 
                        $exp_url
                    );
    }
    return wp_nonce_url( $exp_data, 'exp_data', 'exp_data_nonce' );
}

echo '<div class="wrap setting_mode">';
?>

<div class="app_users_list-wrap new_user">
    <!-- Loader div -->
    <div class="img-hide">
        <img src="<?php echo BADEM_CORE_IMG; ?>transparent-loading.gif" alt="Loader-gif">
    </div>

    <div class="row">
        <div class="col-md-10 col-sm-8">
            <h1>App Users List</h1>
            <h6 class="sub_title">Showing all active users from various apps and platforms</h6>
        </div>
        <div class="col-md-2 col-sm-4 float-right">
            <div class="div-right">
                <a href="#" data-toggle="modal" data-target="#import_modal" class="button">Import/Export</a>
            </div>
        </div>
    </div>
    <!-- Import/Export button -->

    <div class="row new_user_row">
        <div class="col-md-2 col-sm-4">
            <select id='searchBystatus'>
                <option value='active' selected> Install </option>
                <option value='inactive'> Uninstall </option>
            </select>
        </div>
        <div class="col-md-2 col-sm-4">
            <select id='searchByPlatform'>
                
            </select>
        </div>
        <div class="col-md-2 col-sm-4">
            <select id='searchByApps'>
                
            </select>
        </div>
        <div class="col-md-2 col-sm-4">
            <select id='searchByAppsPlans'>
                <option value=''>Apps type</option>
                <option value='free'>Free</option>
                <option value='paid'>Paid</option>
                <option value='trial'>Trial</option>
            </select>
        </div>
        <div class="col-md-2 col-sm-4">
            <div class="date-picker">
                <input type="text" name="" class="date_picker_input" placeholder="From date." id="from_app_date_picker" />
            </div>
        </div>
        <div class="col-md-2 col-sm-4">
            <div class="date-picker">
                <input type="text" name="" class="date_picker_input" placeholder="To date." id="to_app_date_picker" />
            </div>
        </div>
        
  </div>
   
   <!-- Data Table -->
	<table id="app_users_list" class="app_users_list_table responsive appu_datatables">
		<thead>
			<th>ID</th>
            <th>Store ID</th>
            <th>App ID</th>
            <th>Store Name</th>
            <th>App Name</th>
            <th>Name</th>
            <th>Email</th>
            <th>Status</th>
            <th id="created_at">Install/Uninstall Date</th>
            <th>User type</th>
            <th>View Details</th>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>

<!-- Info modal -->
<input type="hidden" name="app_user_id" value="">
<div class="modal fade quick_view_modal" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">User Information</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn_primary" data-dismiss="modal">Close</button>
            </div>
        </div>
      
    </div>
</div>

<!-- Import/Export Modal -->
<div class="modal fade quick_view_modal" id="import_modal" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Data Import & Export</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">

                <div class="container">
                <ul class="nav nav-pills nav-justified">
                    <li class="nav-item">
                      <a id="import_data" class="nav-link active" href="#" data-tbl='import_table'>Import</a>
                    </li>
                    <li class="nav-item">
                        <a id="export_data" class="nav-link" href="#"  data-tbl='export_table'>Export</a>
                    </li>
                </ul>
                </div>
                <div id="import_table">
                    <div class="alert alert-info">
                        <strong>Note:</strong> please colunm value define like this {<br>environment( 1 = sandbox mode, 2 = live mode ) ,<br> installation date ( <?php echo date( 'Y-m-d H:i:s' );?> format ( 'Y-m-d H:i:s' ) ) }
                  </div>
                    <h6 class="h6_style">Upload csv file only:</h6>
                    <form name="user_import_form" enctype='multipart/form-data' method="post" action="<?php echo generate_nonce_url('import_app_users'); ?>">
                        <div class="custom-file mb-3">
                          <input type="file" id="impFile" placeholder="Select CSV file." name="imp_usr">
     
                        </div>
                        <div class="div-right align">Demo file&nbsp;<a href="<?php echo BADEM_PLUGIN_URL.'assets/exports/users_demo.csv';?>">here</a>
                        </div>
                        <div class="input-group-append">
                          <input type="submit" class="btn btn_primary" name="import_usr" value="Import">
                        </div>
                    </form>
                </div>
                <div id="export_table" style="display: none;">
                    <div class="row field_option">
                        <div class="col-md-3"></div>
                        <div class="custom-control custom-radio col-md-3">
                            <input type="radio" class="custom-control-input" id="customControlValidation2" name="field" value="all" checked required>
                            <label class="custom-control-label" for="customControlValidation2"> All colunm </label>
                        </div>
                        <div class="custom-control custom-radio col-md-3">
                            <input type="radio" class="custom-control-input" id="customControlValidation3" data-check="tick_field" name="field" value="field" required>
                            <label class="custom-control-label" for="customControlValidation3"> Specific colunm </label>
                        </div>
                    </div>
                    <div class="row field_style">
                        <div class="col-md-2">
                            <input type="checkbox" id="usr_id" name="usr_id" value="id">
                            <label for="usr_id"> ID </label>
                        </div>
                        <div class="col-md-2">
                            <input type="checkbox" id="str_id" name="str_id" value="store_id">
                            <label for="str_id"> Store Id </label>
                        </div>
                        <div class="col-md-3">
                            <input type="checkbox" id="ap_name" name="ap_name" value="app_name">
                            <label for="ap_name"> App Name </label>
                        </div>
                        <div class="col-md-2">
                            <input type="checkbox" id="usr" name="usr" value="name">
                            <label for="usr"> Name </label>
                        </div>
                        <div class="col-md-2">
                            <input type="checkbox" id="mail" name="mail" value="email">
                            <label for="mail"> Email </label>
                        </div>
                    </div>
                    
                    <h6 class="h6_style">Export data from the table:</h6>
                    <a id="exp_query_click" data-dismiss="modal" href="<?php echo generate_nonce_url('export_app_users'); ?>" class="btn btn_primary">Export</a>
                </div>

            </div>

        </div>
      
    </div>
</div>

<?php

echo '</div>';
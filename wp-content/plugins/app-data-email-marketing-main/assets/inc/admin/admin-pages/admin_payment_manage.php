<?php

/*****
    Description: Ajax handlers file for handling js code without page refresh.
    Author: BitCot
    Created: 30-Mar-2021
    Modify: 01-Apr-2021

*****/


function create_nonce_url($tact, $naname, $nname){
	if($tact){
        $exp_url = admin_url().'admin.php?page=payments_info';
        $exp_data = add_query_arg( 
                        array( 
                            'action' => $tact,
                        ), 
                        $exp_url
                    );
    }
    return wp_nonce_url( $exp_data, $naname, $nname );
}

?>

<div class="wrap setting_mode">
	<h1>Payment Information</h1>
            <!-- Loader div -->
    		<div class="img-hide">
    			<img src="<?php echo BADEM_CORE_IMG; ?>transparent-loading.gif" alt="Loader-gif">
    		</div>

            <div class="row pmanage_row">
                <div class="col-md-2 col-sm-4">
                    <select id="app_plaform">
                        <option value="">All platforms</option>
                        <?php echo selection_platform_list(); ?>
                    </select>
                </div>
                <div class="col-md-2 col-sm-4">
                    <select id="apps_info">        
                    </select>            
                </div>
                <div class="col-md-2 col-sm-4">
                    <select id="payment_status">
                        <option value=""> All </option>
                        <option value="paid"> Paid </option>
                        <option value="failed"> Failed </option>
                        <option value="in-process"> Under Process </option>
                    </select>
                </div>
                <div class="col-md-2 col-sm-4">
                    <div class="date-picker"> 
                        <input type="text" name="" class="date_picker_input" placeholder="From date." id="from_pay_date_picker" /> 
                    </div>
                </div>
                <div class="col-md-2 col-sm-4">
                    <div class="date-picker"> 
                        <input type="text" name="" class="date_picker_input" placeholder="To date." id="to_pay_date_picker" /> 
                    </div>
                </div>
                <div class="col-md-2 col-sm-4">
                    <div class="div-right">
                        <a href="#" data-toggle="modal" data-target="#import_modals" class="button">Import/Export</a>
                    </div>
                </div>
            </div>

    	<table id="pay_data_table" class="table table-hover dt-responsive nowrap">
    		<thead>
    		    <tr>
    		    	<th> App Id </th>
    		    	<th> App Name </th>
    		    	<th> User </th>
    		    	<th> Email </th>
    		    	<th> Payment Date </th>
    		    	<th> Expiry Date </th>
    				<th> Payement Status </th>
    				<th> Amount </th>	    	
    		    	<th> Renewal Date </th>
    		    </tr>
    		</thead>
    		<tbody>
            </tbody>
    	</table>


    <!-- Import/Export modal -->
    <div class="modal fade quick_view_modal" id="import_modals" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Data Import & Export</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">

                    <div class="container">
                    <ul class="nav nav-pills nav-justified">
                        <li class="nav-item">
                          <a id="import_data" class="nav-link active" href="#" data-tbl='import_table'>Import</a>
                        </li>
                        <li class="nav-item">
                            <a id="export_data" class="nav-link" href="#"  data-tbl='export_table'>Export</a>
                        </li>
                    </ul>
                    </div>
                    <div id="import_table">
                        <div class="alert alert-info">
                            <strong>Note:</strong> please colunm date define like this {<br> Created at ( <?php echo date( 'Y-m-d H:i:s' );?> format ( 'Y-m-d H:i:s' ) ) }
                        </div>
                        <h6 class="h6_style">Upload csv file only:</h6>
                        <form enctype='multipart/form-data' name="paymentinfo_import_form"
                        method="post" action="<?php echo create_nonce_url('import_payment', 'imp_paid', 'imp_paid_nonce'); ?>">
                            <div class="custom-file mb-3">
                              <input type="file" id="impFile" name="imp_pydata">

                            </div>
                            <div class="div-right align">Demo file&nbsp;<a href="<?php echo BADEM_PLUGIN_URL.'assets/exports/payments_demo.csv';?>">here</a>
                            </div>
                            <div class="input-group-append">
                              <input type="submit" class="btn btn_primary" name="import_pdata" value="Import" />
                            </div>
                        </form>
                    </div>
                    <div id="export_table" style="display: none;">
                        <h6>Export data from the table:</h6>
                        <a id="paid_export" data-dismiss="modal" href="<?php echo create_nonce_url('export_payment', 'exp_paid', 'exp_paid_nonce');?>" class="btn btn_primary">
                            Export
                        </a>
                    </div>

                </div>

            </div>
          
        </div>
    </div>
</div>


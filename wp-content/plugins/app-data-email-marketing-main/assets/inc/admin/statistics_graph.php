<?php

/*****
	Description: Statistics page for displayed dynamically data in statistics graphs.
	Author: BitCot
	Created: 22-Mar-2021
	Modify: 06-apr-2021
*****/
?>

<div class="wrap setting_mode">
	<h1 class="text-center"> Statistics Graph </h1>

	<div class="row">

		<!-- <div class="col-md-2 col-sm-4 text-right">
			<select id="static_graph" class="statics_graph">
				
			</select>
		</div> -->
		<div class="col-sm-6"></div>
		<div class="col-md-2 col-sm-4">
			<div class="date-picker">
              <input type="text" name="from_filter" class="date_picker_input" placeholder="From date." id="from_filter">
            </div>
		</div>

		<div class="col-md-2 col-sm-4">
			<div class="date-picker">
              <input type="text" name="to_filter" class="date_picker_input" placeholder="To date." id="to_filter">
            </div>
		</div>

	</div>

	<div class="row">

		<div class="col-md-2 tabs">
			<div class="container">
				<div class="col-xl-2">
					<ul class="nav nav-pills nav-stacked flex-column">
						<a id="year" data-filter = "year" href="" data-toggle="pill"><li> Year </li></a>
						<a id="half_year" data-filter = "half_year" href="" data-toggle="pill"><li> Last 6 Month </li></a>
						<a id="three_month" data-filter = "three_month" href="" data-toggle="pill"><li> Last 3 Month </li></a>
						<a id="week" data-filter = "week" href="" data-toggle="pill"><li> Current Week </li></a>
					</ul>
				</div>
			</div>
		</div>

		<div class="col-md-8">
			<div class="colmask leftmenu">
			    <div class="colleft">
			        <div class="col1" id="example-content">
			       
			    		<div id="stat_graph"></div>

			        </div>
			         
			    </div>
			</div>
		</div>
	</div>

	<div class="row mx-md-n5">
	  <div class="col px-md-5"><div class="p-5"></div></div>
	</div>

	<div class="row">

		<div class="col-sm-6"></div>
		<div class="col-md-2 col-sm-4">
			<div class="date-picker">
              <input type="text" name="ap_from_filter" class="date_picker_input" placeholder="From date." id="ap_from_filter">
            </div>
		</div>

		<div class="col-md-2 col-sm-4">
			<div class="date-picker">
              <input type="text" name="ap_to_filter" class="date_picker_input" placeholder="To date." id="ap_to_filter">
            </div>
		</div>

	</div>
	
	<div class="row">
		<div class="col-md-2 tabs">
			<div class="container">
				<div class="col-xl-2">
					<ul class="nav nav-pills nav-stacked flex-column">
						<a id="ap_year" data-filter = "ap_year" href="" data-toggle="pill"><li> Year </li></a>
						<a id="ap_half" data-filter = "ap_half" href="" data-toggle="pill"><li> Last 6 Month </li></a>
						<a id="ap_quarter" data-filter = "ap_quarter" href="" data-toggle="pill"><li> Last 3 Month </li></a>
						<a id="ap_week" data-filter = "ap_week" href="" data-toggle="pill"><li> Current Week </li></a>
					</ul>
				</div>
			</div>
		</div>

		<div class="col-md-8">
			<div class="colmask leftmenu">
			    <div class="colleft">
			        <div class="col1" id="example-content">
			       
			    		<div id="user_graph"></div>

			        </div>
			         
			    </div>
			</div>
		</div>
	</div>

</div>

<script>

/*var static_graph = "#static_graph";*/
var user_graph = "#user_graph";
var stat_graph = "#stat_graph";
/* For revenue graph */
var gr_year = "#year";
var gr_half_year = "#half_year";
var gr_three_month = "#three_month";
var gr_week = "#week";
var gr_from_filter = "#from_filter";
var gr_to_filter = "#to_filter";
/* For user graph */
var ap_year = "#ap_year";
var ap_half = "#ap_half";
var ap_quarter = "#ap_quarter";
var ap_week = "#ap_week";
var ap_from_filter = "#ap_from_filter";
var ap_to_filter = "#ap_to_filter";


( function ( $ )

{
	$ ( document ).ready( function ()
	{
		var fltrs, $a = $( ".tabs li" );
		var graph_arr = new Array(),
		gr_clicks = new Array(),
		ap_clicks = new Array(),
		fltr_req = new Array(),
		data_fltr = new Array(),
		gr_data = new Array();

		$(function() {
			
			$a.click(function() {
				$a.removeClass( "active" );
				$( this ).addClass("active");
			});
		});

		if( stat_graph.length )
		{
			rev_fltr = new Array();
			gr_clicks = {
						 "year" : gr_year,
						 "half" : gr_half_year,
						 "quarter" : gr_three_month,
						 "week" : gr_week,
						 "from_filter" : gr_from_filter,
						 "to_filter" : gr_to_filter };

			rev_fltr = { "id" : stat_graph, "name" : "stat_graph", "title" : "Revenue Data" };
			
				dynamic_value_by_id( gr_clicks );
		}

		if( user_graph.length )
		{
			usr_fltr = new Array();
			ap_clicks = {
						 "year" : ap_year,
						 "half" : ap_half,
						 "quarter" : ap_quarter,
						 "week" : ap_week,
						 "from_filter" : ap_from_filter,
						 "to_filter" : ap_to_filter };

			usr_fltr = { "id" : user_graph, "name" : "user_graph", "title" : "User Data" };
			
			dynamic_value_by_id( ap_clicks );
		}
		
		graph_statistics_data();

		function dynamic_value_by_id( fltrs )
		{
			var year, half_year, three_month, week, from_filter, to_filter;
			if( fltrs.year.length )
			{
				year = fltrs.year;
				$( 'body' ).on( 'click', year, function() 
				{
					$( fltrs.from_filter ).val('');
					$( fltrs.to_filter ).val('');
					if( $( this ).attr( 'data-filter' ) == 'year' || $( this ).attr( 'data-filter' ) == 'ap_year' )
					{
						var cur_day = new Date;
						fltr_req = { "attr" : $( this ).attr( 'data-filter' ),
									 "year" : new Date( cur_day.setMonth( cur_day.getMonth() - 12 ) ) };
						graph_statistics_data();
					}
					
				});
			}

			if( fltrs.half.length )
			{
				half_year = fltrs.half;
				$( 'body' ).on( 'click', half_year, function() 
				{
					$( fltrs.from_filter ).val('');
					$( fltrs.to_filter ).val('');
					if( $( this ).attr( 'data-filter' ) == 'half_year' || $( this ).attr( 'data-filter' ) == 'ap_half' )
					{
						var cur_day = new Date;
						fltr_req = {"attr" : $( this ).attr( 'data-filter' ), 
									"half_year" : new Date( cur_day.setMonth( cur_day.getMonth() - 6 ) ) };
						graph_statistics_data();
					}
				});

			}

			if( fltrs.quarter.length )
			{
				three_month = fltrs.quarter;
				$( 'body' ).on( 'click', three_month, function() 
				{
					if( $( this ).attr( 'data-filter' ) == 'three_month' || $( this ).attr( 'data-filter' ) == 'ap_quarter' )
					{
						$( fltrs.from_filter ).val('');
						$( fltrs.to_filter ).val('');
						var cur_day = new Date;
						fltr_req = {"attr" : $( this ).attr( 'data-filter' ), 
									"quarter" : new Date( cur_day.setMonth( cur_day.getMonth() - 3 ) ) };
						graph_statistics_data();
					}
				});
			}
			
			if( fltrs.week.length )
			{
				week = fltrs.week;
				$( 'body' ).on( 'click', week, function() 
				{
					if( $( this ).attr( 'data-filter' ) == 'week' || $( this ).attr( 'data-filter' ) == 'ap_week' )
					{
						$( fltrs.from_filter ).val('');
						$( fltrs.to_filter ).val('');
						var cur_day = new Date;
						fltr_req = {"attr" : $( this ).attr( 'data-filter' ),
									"week" : new Date( cur_day.setDate( cur_day.getDate() - 6 ) ) };
						graph_statistics_data();
					}
				});
			}

			if( fltrs.from_filter.length && fltrs.to_filter.length )
			{
				from_filter = fltrs.from_filter;
				to_filter = fltrs.to_filter;

				$( 'body' ).on( 'change', from_filter, function()
				{
					$a.removeClass( "active" );
					var to_id = '', to_fltr_data = '';
					if( $( to_filter ).val() )
					{
						to_id = to_filter;
						to_fltr_data = $( to_filter ).val();
					}
					fltr_req = {"from_id" : from_filter, "to_id" : to_id,
								"from_filter" : $( from_filter ).val(), "to_filter" : to_fltr_data };
					graph_statistics_data();
				});

				$( 'body' ).on( 'change', to_filter, function()
				{
					$a.removeClass( "active" );
					var from_id = '', from_fltr_data = '';
					if( $( from_filter ).val() )
					{
						from_id = from_filter;
						from_fltr_data = $( from_filter ).val();
					}
					fltr_req = {"to_id" : to_filter, "from_id" : from_id,
								"to_filter" : $( to_filter ).val(), "from_filter" : from_fltr_data };
					graph_statistics_data();
				});
			}

		}

		function graph_statistics_data()
		{
			jQuery.post( ajaxurl,
				data = 
				{
					action: 'stat_graph_data',
					rev: rev_fltr,
					usr: usr_fltr,
					filter: fltr_req,
					cache: false,
				},
				function ( graph )
				{
					/*console.log( graph );*/
					var gr_data = JSON.parse( graph );
					/*console.log(gr_data);*/
					if( gr_data.rev_id == '#stat_graph' )
					{
						data_fltr.id = gr_data.rev_id;
						data_fltr.name = gr_data.rev_res.name;
						data_fltr.title = gr_data.rev_res.title;
						data_fltr.graph = gr_data.rev_graph;
						data_fltr.yaxis = "$%.2f&emsp;";
						
						console.log(data_fltr);
						draw_statistics_graph( data_fltr );	
					}

					if( gr_data.usr_id == '#user_graph' )
					{
						data_fltr.id = gr_data.usr_id;
						data_fltr.name = gr_data.usr_res.name;
						data_fltr.title = gr_data.usr_res.title;
						data_fltr.graph = gr_data.usr_graph;
						data_fltr.yaxis = "%.2i&emsp;";
						
						console.log(data_fltr);
						draw_statistics_graph( data_fltr );	
					}
					/*console.log(data_fltr);*/
				}
			);
		}

		function draw_statistics_graph( gr_data )
		{
			if( gr_data )
			{

				$( gr_data.id ).empty();
				var id_name = gr_data.name;
				
				var statics_graph = gr_data.graph;
				var plot2 = $.jqplot( id_name, [statics_graph],
				{
				    title:gr_data.title,
				    animate: true,
				    seriesDefaults:{ renderer:$.jqplot.BarRenderer, pointLabels: { show: true } },
				    /*seriesDefaults: {shadow: true, renderer: jQuery.jqplot.PieRenderer, rendererOptions: { showDataLabels: true } }, */

				    axes:
				    {
				      	xaxis:
				      	{
				        renderer:$.jqplot.DateAxisRenderer,
				        tickRenderer: $.jqplot.CanvasAxisTickRenderer,
					        tickOptions:
					        {
					          formatString:'%b&nbsp;%Y'
					        }
				      	},
				      	yaxis:
				      	{
				      		renderer: $.jqplot.LogAxisRenderer,
				      		pad: 1.3,
					        tickOptions:
					        {
					          formatString:gr_data.yaxis
					        }	
				      	}
				    },

				    highlighter:
				    {
				      	show: false,
						/*sizeAdjust: 7.5*/
				    },

				    cursor:
				    {
				      show: true,
				      /*tooltipLocation:'sw'*/
				    }
				} );
			}
		}  

	} );

} )( jQuery );

</script>


<!--if( fltrs.to_filter.length )
			{
				to_filter = fltrs.to_filter;
				$( 'body' ).on( 'change', to_filter, function()
				{
					$a.removeClass( "active" );
					fltr_req = { "from_filter" : $( gr_from_filter ).val(), "to_filter" : $( gr_to_filter ).val() };
					graph_statistics_data();
				});
			}

		$( 'body' ).on( 'click', gr_year, function() 
			{
				if( $( this ).attr( 'data-filter' ) == 'year' )
				{
					graph_arr = {};
					var cur_day = new Date;
					graph_arr = { "year" : new Date( cur_day.setMonth( cur_day.getMonth() - 12 ) ) };
					graph_statistics_data();	
				}
				
			});

		$( 'body' ).on( 'click', gr_half_year, function() 
			{
				if( $( this ).attr( 'data-filter' ) == 'half_year' )
				{
					graph_arr = {};
					var cur_day = new Date;
					graph_arr = { "half_year" : new Date( cur_day.setMonth( cur_day.getMonth() - 6 ) ) };
					graph_statistics_data();
				}
			});

		$( 'body' ).on( 'click', gr_three_month, function() 
			{
				if( $( this ).attr( 'data-filter' ) == 'three_month' )
				{
					graph_arr = {};
					var cur_day = new Date;
					graph_arr = { "quarter" : new Date( cur_day.setMonth( cur_day.getMonth() - 3 ) ) };
					graph_statistics_data();
				}
			});

		$( 'body' ).on( 'click', gr_week, function() 
			{
				if( $( this ).attr( 'data-filter' ) == 'week' )
				{
					graph_arr = {};
					var cur_day = new Date;
					graph_arr = { "week" : new Date( cur_day.setDate( cur_day.getDate() - 6 ) ) };
					graph_statistics_data();
				}
			});

		$( 'body' ).on( 'change', gr_from_filter, function()
			{
				$a.removeClass( "active" );
				graph_arr = { "from_filter" : $( gr_from_filter ).val(), "to_filter" : $( gr_to_filter ).val() };
				graph_statistics_data();
				
			});
		
		$( 'body' ).on( 'change', gr_to_filter, function()
			{
				$a.removeClass( "active" );
				graph_arr = { "from_filter" : $( gr_from_filter ).val(), "to_filter" : $( gr_to_filter ).val() };
				graph_statistics_data();
			});

<div class="tabs">
		<div class="container">
			<div class="row">
				<div class="col-xl-2">
					<ul class="nav nav-pills nav-stacked flex-column">
						<li class="active"><a href="#tab_a" data-toggle="pill"> Year </a></li>
						<li><a href="#tab_b" data-toggle="pill"> Half Year </a></li>
						<li><a href="#tab_c" data-toggle="pill"> Quarter Year </a></li>
						<li><a href="#tab_d" data-toggle="pill"> Weekly </a></li>
					</ul>
				</div>
				<div class="col-xl-6">
					<div class="tab-content">
						<div class="tab-pane active" id="tab_a">
							<h3>First tab with soft transitioning effect.</h3>
							<p>American Builders Inc. is your full service general contractor. We have been helping 
								clients throughout Eastern North Carolina with their construction needs since 1996.
								We take pride in understanding our clients' needs, making their construction experience 
								as worry free as possible and only delivering a finished product that will withstand the
							test of time. </p>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod 
							bibendum laoreet.</p>
						</div>
						<div class="tab-pane" id="tab_b">
							<h3>Second tab with soft transitioning effect.</h3>
							<p>We maintain a reputation for effective communication and collaboration between our 
								team and clients to minimize surprises and ensure precise project delivery. Lorem ipsum 
								dolor sit amet, consectetur adipiscing elit. Aenean euismod 
								bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra 
								justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque 
							penatibus et magnis dis parturient montes.</p>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod 
							bibendum laoreet.</p>
						</div>
						<div class="tab-pane" id="tab_c">
							<h3>Third tab with soft transitioning effect.</h3>
							<p>Thank you for taking the time to consider American Builders Inc. as your general
								contractor for any and all of your commercial, residential, insurance restoration and metal 
								building needs. Please feel free to explore our website, and be sure to click on the
							Facebook link at the bottom of this page and like us on Facebook.</p>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod 
							bibendum laoreet.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div> -->
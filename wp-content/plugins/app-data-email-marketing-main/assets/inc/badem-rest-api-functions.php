<?php
/*
*
*	***** Bitcot Apps Data & Email Marketing *****
*
*	Rest API Functions
*	
*/
/*If this file is called directly, abort.*/ 
if ( ! defined( 'WPINC' ) ) {die;}  /*end if*/
/*
*
* Custom REST API Routes
*
*/

class Custom_REST_Routs_Controller extends WP_REST_Controller {
	public function register_routes() {
	    $namespace = 'maillist/v1';
	    $add_user_path = 'add-user';
	    $unsubscribe_user_path = 'unsubscribe-user';

	    register_rest_route( $namespace, '/' . 'app-data', [
	      	array(
		        'methods'             => 'POST',
		        'callback'            => array( $this, 'GetActionbyUser' ),
		        'permission_callback' => array( $this, 'permissions_check' ),
		        /*'args'                => $this->get_endpoint_args_for_item_schema( true ),*/
	    	),

	        ]
	    );  
    }

    public function permissions_check($request) {
		return is_user_logged_in();
	    /*return true;*/
	}

	public function GetActionbyUser ($request) {
		$args = json_decode( $request->get_body(), true );

		if( isset($args['action'] ) && !empty( $args['action'] ) ) {
			$UsrAction = $args['action'];
			$response = '';
			switch ($UsrAction){
				case 'add-user' :
				$response = $this->create_user($args);
				break;

				case 'update-user' :
				$response = $this->unsubscribe_user($args);
				break;

				case 'make-payment' :
				$response = $this->app_payments_information($args);
				break;

				case 'get_appid' :
				$response = $this->appid_return_by_value($args);
				break;
							
			}
			return $response;
		}else{
			return new WP_REST_Response( array( 'response' => 'fail', 'body_response' => 'Your Request are rejected, please set action parameter.'), 400);
		}
	}

	public function create_user($args) {

	    $u = array();
	    $u['fname'] = $args['name'];
	    $u['lname'] = '';
	    $u['email'] = $args['email'];


	    $list_ids = array();

	    $nlname = badem_NameConversion($args['app_platform']);

	    $acmail_list = badem_get_email_list_id( $nlname[$args['app_platform']].'-'.$args['app_name'].'-active' );
	    $plmail_list = badem_get_email_list_id( $nlname[$args['app_platform']].'-'.$args['app_name'] );

	    if($acmail_list && $plmail_list ){
	    	$list_ids[] = $acmail_list;
	    	$list_ids[] = $plmail_list;
	    		
	    }else{
	    	$e_list = badem_create_get_list_id($args['app_platform'], $args['app_id'], $args['app_name'], $args['app_status']);

		    for ($li=0; $li < count($e_list); $li++) { 
		    	$list_ids[] = $e_list[$li]['id'];
		    }
	    	
	    }

	    $db_op = new app_installation_by_user;
	    $db_op_res = $db_op->app_new_users( $args);

	    if( isset( $db_op_res['status'] ) && $db_op_res['status'] == 'success' ){

	    	if( $args['email'] ) {
				$giname = badem_NameConversion($args['app_platform']);
	    		$list_ids[] = badem_get_email_list_id( $giname[$args['app_platform']] );
	    		$list_ids[] = badem_get_email_list_id( $giname[$args['app_platform']].' Active' );
	    		
			    $error = badem_mailPoet_subscribe_to_list( $u, $list_ids );
	    	}
	    }else{
	    	return new WP_REST_Response( array( 'response' => 'fail', 'body_response' => $db_op_res['msg']), 400);
	    }

	    if($error !== true){
	    	return new WP_REST_Response( array( 'response' => 'fail', 'body_response' => $error), 400);
	    }else{
	    	return new WP_REST_Response( array(
									        'response' => 'success',
									        'body_response' => 'successfully subscribe to the mail list'
									      	) , 200
							    		);
	    }
	}

	public function unsubscribe_user($args) {

	    if (empty($args)) {
	        return new WP_Error( 'empty_request', 'there is no data in your request', array( 'status' => 400 ) );
	    }
	    
	    $vlname = badem_NameConversion($args['app_platform']);
	    $valid_mail_list = badem_get_email_list_id( $vlname[$args['app_platform']].'-'.$args['app_name'].'-inactive' );
	    if(!$valid_mail_list){
	    	badem_create_get_list_id($args['app_platform'], $args['app_id'], $args['app_name'], $args['app_status']);	

	    }

	    $db_udp = new app_updation_by_user;
	    $db_upd_res = $db_udp->app_data_update( $args);

	    $u = [];
	    $u['email'] = $args['email'];
	    if(isset( $db_upd_res['status'] ) && $db_upd_res['status'] == 'success' ){

	    	if($args['app_status'] == 'active'){
	    		
	    		$list_ids = array();
	    		$ulist_ids = array();
	    		$giname = badem_NameConversion($args['app_platform']);
	    		$list_ids[] = badem_get_email_list_id( $giname[$args['app_platform']] );
	    		$list_ids[] = badem_get_email_list_id( $giname[$args['app_platform']].' Active' );
	    		$list_ids[] = badem_get_email_list_id( $giname[$args['app_platform']].'-'.$args['app_name'].'-active' );

    			$ulist_ids[] = badem_get_email_list_id( $giname[$args['app_platform']].' Inactive' );
    			$ulist_ids[] = badem_get_email_list_id( $giname[$args['app_platform']].'-'.$args['app_name'].'-inactive' );

    			$add_mail_list = badem_mailPoet_subscribe_to_list( $u, $list_ids );
    			$remove_mail_list = badem_mailPoet_unsubscribe_from_list( $u, $ulist_ids );
    			if( $add_mail_list && $remove_mail_list ){
    				return new WP_REST_Response( array(
									        'response' => 'success',
									        'body_response' => 'Welcome back to use our services again.'
									      ) , 200
							    		); 	
    			}
    			
	    	}else{

		    	if( empty( $u['email'] ) ) {
		    		$u['email'] = $db_upd_res['email'];
		    	}

		    	$list_ids = array();
		    	$add_mail = array();
		    	$giname = badem_NameConversion($args['app_platform']);
	    		$list_ids[] = badem_get_email_list_id( $giname[$args['app_platform']].' Active' );
	    		$list_ids[] = badem_get_email_list_id( $giname[$args['app_platform']].'-'.$args['app_name'].'-active' );

	    		$add_mail[] = badem_get_email_list_id( $giname[$args['app_platform']].' Inactive' );
	    		$add_mail[] = badem_get_email_list_id( $giname[$args['app_platform']].'-'.$args['app_name'].'-inactive' );
	    		
			    $inMail = badem_mailPoet_subscribe_to_list( $u, $add_mail );
			    $unMail = badem_mailPoet_unsubscribe_from_list( $u, $list_ids );
			    if( $inMail && $unMail ){
			    	$error = true;
			    }else{
			    	$error = false;
			    }
			}
	    }else{
	    	return new WP_REST_Response( array( 'response' => 'fail', 'body_response' => $db_upd_res['msg']), 400);
	    }
	    if($error !== true){
	    	return new WP_REST_Response( array( 'response' => 'fail', 'body_response' => $error), 400);
	    }else{
	    	return new WP_REST_Response( array(
									        'response' => 'success',
									        'body_response' => 'successfully unsubscribe from the lists'
									      ) , 200
							    		);	
	    }
	    
	}

	public function app_payments_information($args){
		if (empty($args)) {
	        return new WP_Error( 'empty_request', 'there is no data in your request', array( 'status' => 400 ) );
	    }

	    $rcvd_payments = new badem_get_app_payments;
	    $app_payments = $rcvd_payments->app_payments_processing_data( $args );

	    if(isset($app_payments['status']) && $app_payments['status'] == 'success'){
	    	return new WP_REST_Response( array(
									        'response' => 'success',
									        'body_response' => 'Your payment has been received successfully.'
									      ) , 200
							    		);	
	    }else{
	    	return new WP_REST_Response( array( 'response' => 'fail', 'body_response' => $app_payments['emsg']), 400);
	    }
	}

	public function appid_return_by_value($args)
	{
		if (empty($args))
		{
	        return new WP_Error( 'empty_request', 'there is no data in your request', array( 'status' => 400 ) );
	    }
	    $appid_call = get_appid_by_platform( $args['platform'], $args['app_name'] );
	    if( $appid_call )
	    {
	    	return new WP_REST_Response( array(
									        'app_id' => $appid_call->id
									      ) , 200
							    		);	
	    }else
		   {
		    return new WP_REST_Response( array( 'response' => 'fail', 'body_response' => 'please provide correct data.'), 400);
		   }
	}
}

add_action('rest_api_init', function () {
   $custom_controller = new Custom_REST_Routs_Controller();
    $custom_controller->register_routes();
});


	/*case 'app-plans' :
	$response = $this->app_plan_subscription($args);
	break;*/

	/*public function app_plan_subscription($args){
		if (empty($args)) {
	        return new WP_Error( 'empty_request', 'there is no data in your request', array( 'status' => 400 ) );
	    }
	    
	    $app_plans = new app_plan_subscription_function;
	    $db_app_plans = $app_plans->apps_plan_subscriptions( $args);

	    if(isset($db_app_plans['status']) && $db_app_plans['status'] == 'success'){
	    	return new WP_REST_Response( array(
									        'response' => 'success',
									        'body_response' => 'successfully added apps plan in the data'
									      ) , 200
							    		);	
	    }else{
	    	return new WP_REST_Response( array( 'response' => 'fail', 'body_response' => $db_app_plans['mesg']), 400);
	    }
	}*/
				
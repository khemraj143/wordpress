<?php

/*****
	Description: Database related functions for data modification in database.
	Author: BitCot
	Created: 24-Mar-2021
	Modify: 26-mar-2021

*****/


/** App data add, modify from Database **/
add_action( 'init' , function()
{
	if(isset($_POST['add']))
	{

		if ( isset( $_GET['action'] ) && $_GET['action'] == 'add_apps' )
		{
			add_action('admin_init', function()
			{

				if ( wp_verify_nonce( $_GET['new_apps'], 'apps_action' ) )
				{
					global $wpdb;
					if( !$_POST['app_id'] )
					{
						$app_name = $_POST['app_name'];
						$patform = $_POST['app_platform'];
						if( $app_name && $patform )
						{
							$app_data = array(
										'app_name' => $_POST['app_name'],
										'platform' => $_POST['app_platform']
										);
							$app_insert = $wpdb->insert( $wpdb->prefix.'apps_platform', $app_data );
							$app_success = $wpdb->insert_id;
							
							if( $app_success )
							{
								?>
								<div class="notice notice-success is-dismissible">
					                <p><?php _e('New data added successfully.', 'BADEM') ?></p>
					            </div>
					            <?php
							}else
								{
								?>
						            <div class="notice notice-error">
						                <p><?php _e('App name '.$_POST['app_name']. ', platform '.$_POST['app_platform']. ' has not add in our data.', 'BADEM') ?></p>
						            </div>
								 
								<?php
								}
						}	
					}else
						{
							if( $_POST['app_name'] && $_POST['app_platform'] )
							{
								global $wpdb;
								$db_table = $wpdb->prefix.'apps_platform';
								$row_update_data = array(
													'app_name' => $_POST['app_name'],
													'platform' => $_POST['app_platform']
													);
								$where_in_row = array(
													'id' => $_POST['app_id']
													);
								$app_update = $wpdb->update( $db_table, $row_update_data, $where_in_row );
								if( $app_update )
								{
									?>
										<div class="notice notice-success is-dismissible">
							                <p><?php _e('Data updated successfully.', 'BADEM') ?></p>
							            </div>
							        <?php
								}else
									{
										?>
								            <div class="notice notice-error">
								                <p><?php _e('App name '.$_POST['app_name']. ', platform '.$_POST['app_platform']. ' has not update in our data.', 'BADEM') ?></p>
								            </div>
										 
										<?php
									}
							}else
								{
									?>
							            <div class="notice notice-error">
							                <p><?php _e('App name '.$_POST['update_name']. ', platform '.$_POST['update_platform']. ' has not provide.', 'BADEM') ?></p>
							            </div>
									 
									<?php
								}		
						}
					
					
					
				}
			} );
		}
	}

} );

/** User data import between csv to database **/
add_action( 'init', function(){

    if(isset( $_POST['import_usr'] ) ){

		if ( isset( $_GET['action'] ) && $_GET['action'] == 'import_app_users' )  {

			add_action('admin_init', function(){

				$imp_file = $_FILES['imp_usr']['name'];
				$imp_tmp = $_FILES['imp_usr']['tmp_name'];

				if ( wp_verify_nonce( $_GET['exp_data_nonce'], 'exp_data' ) ) {
					
					$usr_insert = new Custom_REST_Routs_Controller;

					$extension = pathinfo($imp_file, PATHINFO_EXTENSION);
					if($imp_file && $extension == 'csv'){

						$impReadcsv = fopen($imp_tmp, 'r');
						$sr = 0;
						$csv_msg = array();
						while ( ($impCsvdata = fgetcsv($impReadcsv)) !== FALSE ) {

							if($sr > 0){
								$csv = array(
										'id' => $impCsvdata[0],
										'environment' => $impCsvdata[1],
										'store_id' => $impCsvdata[2],
										'store_url' => $impCsvdata[3],
										'store_name' => $impCsvdata[4],
										'app_platform' => $impCsvdata[5],
										'app_id' => $impCsvdata[6],
										'app_name' => $impCsvdata[7],
										'app_status' => $impCsvdata[8],
										'email' => $impCsvdata[9],
										'name' => $impCsvdata[10],
										'address' => $impCsvdata[11],
										'phone' => $impCsvdata[12],
										'city' => $impCsvdata[13],
										'country' => $impCsvdata[14],
										'zip' => $impCsvdata[15],
										'current_app_plan' => $impCsvdata[16],
										'created_at' => $impCsvdata[17],
										'payment_status_update_at' => $impCsvdata[18],
										'app_status_update_at' => $impCsvdata[19]
										);
							
								if($impCsvdata[8] == 'Install'){
									$csv['app_status'] = 'active';
									$dataInsert = $usr_insert->create_user($csv);
									
									if($dataInsert->data['response'] == 'success'){
										$csv_msg['res'] = "success";
										$csv_msg['emsg'] = "Your Data Inserted";
									}else{

										?>
				 					            <div class="notice notice-error">
									                <p><?php _e( 'Data id = '.$csv['id'].', Store id = '.$csv['store_id'].' not updated.', 'BADEM' ) ?></p>
									            </div>
									 
									    <?php
									}
									
								}
								
								if($impCsvdata[8] == 'Uninstall'){

									$csv['app_status'] = 'inactive';
									
									$user_update = $usr_insert->unsubscribe_user($csv);

										if( $user_update->data['response'] == 'success' )
										{

										 	$csv_msg['res'] = "success";
											$csv_msg['emsg'] = "Your complete data updated successfully.";

										}else{

										 	$csv['app_status'] = 'active';

											$dataInsert = $usr_insert->create_user($csv);

											if($dataInsert->data['response'] == 'success')
											{
												$csv['app_status'] = 'inactive';							
												$dataUpdate = $usr_insert->unsubscribe_user($csv);

												if($dataUpdate->data['response'] == 'success')
												{
													$csv_msg['res'] = "success";
													$csv_msg['emsg'] = "Your complete data updated successfully.";
												
												}else{

													?>
						 					            <div class="notice notice-error">
											                <p><?php _e( 'Data id = '.$csv['id'].', Store id = '.$csv['store_id'].' not updated.', 'BADEM' ) ?></p>
											            </div>
											 
											    	<?php
												}

											}else{

												?>
					 					            <div class="notice notice-error">
										                <p><?php _e( 'Data id = '.$csv['id'].', Store id = '.$csv['store_id'].' not updated.', 'BADEM' ) ?></p>
										            </div>
											 
											    <?php
											}
										}
									
									}
							}
							$sr++;
						}

					}
					
					if($csv_msg['res'] == 'success'){
						?>
 					            <div class="notice notice-success is-dismissible">
					                <p><?php _e('Your data updated successfully.', 'BADEM') ?></p>
					            </div>
					 
					    <?php
					}
					if($csv_msg['res'] == 'fail'){
						?>
 					            <div class="notice notice-error">
					                <p><?php _e( $csv_msg['emsg'], 'BADEM' ) ?></p>
					            </div>
					 
					    <?php
					}

			}else{
				die( __( 'Invalid security nonce.', 'BADEM' ) ); 
				}

			});
		}
	}
});

/** Payment data import between csv to Database **/
add_action( 'init' , function() {
	
	if(isset($_POST['import_pdata'])){
		
		if ( isset( $_GET['action'] ) && $_GET['action'] == 'import_payment' )  {

			add_action('admin_init', function(){

				$imp_pfile = $_FILES['imp_pydata']['name'];
				$imp_ptmp = $_FILES['imp_pydata']['tmp_name'];

				if ( wp_verify_nonce( $_GET['imp_paid_nonce'], 'imp_paid' ) ) {

					$pyData_insert = new Custom_REST_Routs_Controller;		//app_payments_information($args)

					$extn = pathinfo($imp_pfile, PATHINFO_EXTENSION);
					if($imp_pfile && $extn == 'csv'){

						$Readpycsv = fopen($imp_ptmp, 'r');
						$srn = 0;
						$csv_res = array();

						while ( ($FcsvData = fgetcsv($Readpycsv)) !== FALSE ) {
							
							if($srn > 0){

								$pcsv = array(
										'id' => $FcsvData[0],
										'store_id' => $FcsvData[1],
										'app_id' => $FcsvData[2],
										'start_at' => $FcsvData[3],
										'expired_on' => $FcsvData[4],
										'schedule_date' => $FcsvData[5],
										'payment_status' => $FcsvData[6],
										'transaction_id' => $FcsvData[7],
										'amount' => $FcsvData[8],
										'currency' => $FcsvData[9],
										'created_at' => $FcsvData[10],
										);
								$pyData = $pyData_insert->app_payments_information($pcsv);

									if($pyData->data['response'] == 'success'){

										?>
				 					            <div class="notice notice-success is-dismissible">
									                <p><?php _e('Your data updated successfully.', 'BADEM') ?></p>
									            </div>
									 
									    <?php
									}else{

										?>
				 					            <div class="notice notice-error">
									                <p><?php _e('Data id = '.$pcsv['id'].', Store id = '.$pcsv['store_id'].' not updated.', 'BADEM') ?></p>
									            </div>
									 
									    <?php
									}
							}
						$srn++;
						}

					}

				}else{
					die( __( 'Security check', 'BADEM' ) ); 
				}
			});
		    
		}
	}

} );

function get_all_data_form_database( $call_name )
{
	global $wpdb;
	switch( $call_name )
	{
		case 'apps_db' :
		$db_table = $wpdb->prefix.'apps_platform';
		$query_sql = "SELECT * FROM $db_table";
		break;
	}

	return $wpdb->get_results( $query_sql );
}

/** User data export between database to csv **/
if ( isset( $_GET['action'] ) && $_GET['action'] == 'export_app_users' )  {

	add_action('admin_init', function(){

		if ( wp_verify_nonce( $_GET['exp_data_nonce'], 'exp_data' ) ) {

			global $wpdb;
			$get = $_GET;
			$colunm = array();
			$delimiter = ",";
    		$f = fopen('php://memory', 'w');
			$envi = get_option( 'data_access_mode' );

		    $stsQuery = '';
	        if( $get['status'] ){
	            $sts = $get['status'];
	            $stsQuery = "AND app_status = '$sts'";
	        }

	        $pltQuery = '';
	        if( $get['plform'] ){
	            $pltf = $get['plform'];   
	            $pltQuery = "AND app_platform = '$pltf'";
	        }

	        $apsQuery = '';
	        if( $get['apps'] ){
	            $apps = $get['apps'];
	            $apsQuery = "AND app_name = '$apps'";
	        }

	        $plnQuery = '';
	        if( $get['plans'] ){
	            $plns = $get['plans'];
	            $plnQuery = "AND current_app_plan = '$plns'";
	        }

	        $ufrmQuery = '';
	        if( $get['ufrm'] && !$get['uto'] ){
	            $ufrm = $get['ufrm'];
	            $ufrmQuery = "AND Date(created_at) <= '$ufrm'";
	        }

	        $utoQuery = '';
	        if( $get['uto'] && !$get['ufrm'] ){
	            $uto = $get['uto'];
	            $utoQuery = "AND DATE(created_at) <= '$uto'";
	        }
	        
	        $dtbtQuery = '';
	        $ufrom = '';
	        $usto = '';
	        if( $get['ufrm'] && $get['uto'] ){
	        	$ufrom = $get['ufrm'];
	        	$usto = $get['uto'];
	        	$dtbtQuery = "AND DATE(created_at) >= '$ufrom' AND DATE(created_at) <= '$usto'";
	        }
	        /*rad=field&id=id&sid=&app=&usr=&email=*/
	        if( $get['id'] )
	        {
	        	$id = $get['id'];
	        	$colunm[] = $id; 
	        }

	         if( $get['sid'] )
	        {
	        	$sid = $get['sid'];
	        	$colunm[] = $sid;
	        }

	        if( $get['app'] )
	        {
	        	$app = $get['app'];
	        	$colunm[] = $app;
	        }

	        if( $get['usr'] )
	        {
	        	$usr = $get['usr'];
	        	$colunm[] = $usr;
	        }

	        if( $get['email'] )
	        {
	        	$email = $get['email'];
	        	$colunm[] = $email;
	        }

	        if( count($colunm) == 1 )
	        {
	        	$colm = $colunm[0]; 
	        }else{
	 			$get_colm = array();
	        	for ( $i=0; $i < count( $colunm ); $i++ ) { 
	        		$get_colm[] = $colunm[$i];
	        	}
	        	$get_str = implode( " ",$get_colm );
	        	$colm = str_replace( " ", ", ", $get_str );
	        }

	        $tble = $wpdb->prefix.'app_users';

	        if( $get['rad'] == 'all' )
	        {
	        	$results = $wpdb->get_results("SELECT * FROM $tble WHERE environment = '$envi' $stsQuery $pltQuery $apsQuery $plnQuery $ufrmQuery $utoQuery $dtbtQuery ", ARRAY_A);
	        }
	        if( $get['rad'] == 'field' )
	        {
	        	$results = $wpdb->get_results("SELECT $colm FROM $tble WHERE environment = '$envi' $stsQuery $pltQuery $apsQuery $plnQuery $ufrmQuery $utoQuery $dtbtQuery ", ARRAY_A);	
	        }

	        if ( empty( $results ) ) {
	        return;
	        }

	        $fields = str_replace( '_', ' ', array_keys( $results[0] ) );
	        if( $fields[17] ){
	            $fields[17] = 'Installation Date';
	            fputcsv( $f, $fields, $delimiter );    
	        }else{
	        	fputcsv( $f, $fields, $delimiter );
	        }
	        

	        foreach ( $results as $row ) {

	            if( $row['app_status'] == 'active' ){
	                $row['app_status'] = 'Install';
	                fputcsv( $f, $row, $delimiter );    
	            }
	            if( $row['app_status'] == 'inactive' ){
	                $row['app_status'] = 'Uninstall';
	                fputcsv( $f, $row, $delimiter );    
	            }
	            if( !$row['app_status'] ){
	            	fputcsv( $f, $row, $delimiter );	
	            }
	        }

	        fseek($f, 0);
		    fpassthru( $f );
		    
		    if( $sts )
		    {
		    	if( $get['rad'] != 'all'){
			    	$file = $tble.'_'.$get['rad'].'.csv';
			    }else{
			    	$file = $tble.'_'.$sts.'.csv';
			    }
		    }
		    header( 'Content-Description: File Transfer' );
		    header( "Content-type: application/csv; charset=utf-8" );
		    header( "Content-disposition: csv" . date("Y-m-d") . ".csv" );
		    header( "Content-Disposition: attachment; filename=".$file );

		    fclose( $f );
		    exit;

		}else{
			die( __( 'Security check', 'BADEM' ) ); 
		}

	});
    
}


/** Payment data Export between database to csv **/
if ( isset( $_GET['action'] ) && $_GET['action'] == 'export_payment' )  {

	add_action('admin_init', function(){

		if ( wp_verify_nonce( $_GET['exp_paid_nonce'], 'exp_paid' ) ) {

			global $wpdb;
			$delimiter = ",";
    		$f = fopen('php://memory', 'w');
			$envi = get_option( 'data_access_mode' );

		    $plQuery = '';
	        if($_GET['pform']){
	            $plf = $_GET['pform'];
	            $plQuery = "AND u.app_platform = '$plf'";
	        }

	        $apiquery = '';
	        if($_GET['appsinfo']){
	            $api = $_GET['appsinfo'];   
	            $apiquery = "AND u.app_name = '$api'";
	        }

	        $pstquery = '';
	        if($_GET['pystatus']){
	            $pst = $_GET['pystatus'];
	            $pstquery = "AND p.payment_status = '$pst'";
	        }

	        $pfrmQuery = '';
	        if($_GET['payfrm'] && !$_GET['payto']){
	            $pfrm = $_GET['payfrm'];
	            $pfrmQuery = "AND DATE(p.created_at) <= '$pfrm'";
	        }

	        $ptoQuery = '';
	        if($_GET['payto'] && !$_GET['payfrm']){
	            $pto = $_GET['payto'];
	            $ptoQuery = "AND DATE(p.created_at) <= '$pto'";
	        }

	        $btdtQuery = '';
	        $fromp = '';
	        $top = '';
	        if($_GET['payfrm'] && $_GET['payto']){
	        	$fromp = $_GET['payfrm'];
	        	$top = $_GET['payto'];
	            $btdtQuery = "AND DATE(p.created_at) >= '$fromp' AND DATE(p.created_at) <= '$top'";
	        }

	        $py_tbl = $wpdb->prefix.'badem_payments';
	        $us_tbl = $wpdb->prefix.'app_users';
	        $results = $wpdb->get_results("SELECT p.id, p.store_id, p.app_id, p.start_at, p.expired_on, p.schedule_date, p.payment_status, p.transaction_id, p.amount, p.currency, p.created_at FROM $py_tbl p LEFT JOIN $us_tbl u ON p.store_id = u.id WHERE u.environment = '$envi' $plQuery $apiquery $pstquery $pfrmQuery $ptoQuery $btdtQuery", ARRAY_A);

	        if (empty($results)) {
	        return;
	        }

	        $fields = str_replace( '_', ' ', array_keys( $results[0] ) );
	        fputcsv($f, $fields, $delimiter);

	        foreach ($results as $row) {
	            fputcsv($f, $row, $delimiter);    
	        }

	        fseek($f, 0);
		    fpassthru($f);
		    /*die($f);*/
		    header( 'Content-Description: File Transfer' );
		    header("Content-type: text/csv");
		    header("Content-disposition: csv" . date("Y-m-d") . ".csv");
		    header( "Content-Disposition: attachment; filename=".$py_tbl.".csv" );

		    fclose( $f );
		    exit;

		}else{
			die( __( 'Security check', 'BADEM' ) ); 
		}

	});
    
}

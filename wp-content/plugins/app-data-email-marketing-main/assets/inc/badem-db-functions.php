<?php 

/*****
    Description: Ajax handlers file for handling js code without page refresh.
    Author: BitCot
    Created: 30-Mar-2021
    Modify: 31-Mar-2021

*****/
/* If this file is called directly, abort. */
if ( ! defined( 'WPINC' ) ) {die;}  /*end if*/
/*
 * Mailpoet Plugin Changes
 */
class app_installation_by_user{

    private $tname = 'app_users';

    public function app_new_users( $data ){
        global $wpdb;   
        $wp_usr_array = array();
        $response = array();

        $validate_result = $this->ValidateData( $data );
        if( isset($validate_result['status'] ) ){
            return $validate_result;
        }else{
            $wp_usr_array = $validate_result;
        }

        if( $data['store_id'] && $data['app_id'] ){
            $MatchedData = $this->CheckIfEntryExist( $data['store_id'], $data['app_id'] ); 
            if( $MatchedData['status'] ){
                $response['status'] = 'true';
                $response['msg'] = "Your inserted data (eg. Store & App details) already exist.";
                $newMatchedData = $this->NewRowsadd( $data['environment'], $MatchedData['name'], $data['app_id'], $data['app_name'] );
                if( $newMatchedData ){
                    /*return $response;*/
                    $tbname = $wpdb->prefix.$this->tname;
                    $rwdata = array(
                                'environment' =>$wp_usr_array['environment'],
                                'store_name' =>$wp_usr_array['store_name'],
                                'store_url' =>$wp_usr_array['store_url'], 
                                'app_platform' =>$wp_usr_array['app_platform'], 
                                'app_name' =>$wp_usr_array['app_name'], 
                                'app_status' =>$wp_usr_array['app_status'], 
                                'email' =>$wp_usr_array['email'],
                                'name' =>$wp_usr_array['name'], 
                                'address' => $wp_usr_array['address'],
                                'phone' => $wp_usr_array['phone'],
                                'city' => $wp_usr_array['city'],
                                'country' => $wp_usr_array['country'],
                                'zip' => $wp_usr_array['zip'],
                                'current_app_plan' =>$wp_usr_array['current_app_plan'],
                                'created_at' => date('Y-m-d H:i:s'),
                                'payment_status_update_at' => date('Y-m-d H:i:s'),
                                'app_status_update_at' => date('Y-m-d H:i:s')

                            );
                    $where = array(
                                'store_id'=> $wp_usr_array['store_id'],
                                'app_id'=> $wp_usr_array['app_id'],
                            );

                    if( $data['created_at'] ){
                        $rwdata['created_at'] = $data['created_at'];
                    }

                    if( $data['payment_status_update_at'] ){
                        $rwdata['payment_status_update_at'] = $data['payment_status_update_at'];
                    }

                    if( $data['app_status_update_at'] ){
                        $rwdata['app_status_update_at'] = $data['app_status_update_at'];
                    }

                    $update_rows = $wpdb->update( $tbname, $rwdata, $where );

                    if( $update_rows !== false ){
                        $response['status'] = 'success';
                        $response['msg'] = "App installtion has been done.";
                        return $response;
                    }else{
                        $response['status'] = 'false';
                        $response['msg'] = $wpdb->print_error();
                        return $response;
                    }

                }else{
                    $response['status'] = 'false';
                    $response['msg'] = "Apps and platform not added due to backend issue..";
                    return $response;
                }
                
            }else{
                $NewdataLine = $this->NewRowsadd( $data['environment'], $data['app_platform'], $data['app_id'], $data['app_name'] ); 
                if( $NewdataLine ){
                    $tbname = $wpdb->prefix.$this->tname;
                    $NewUserData = array(
                            /*Sandbox mode or live mode data access environment.*/
                            'environment' =>$wp_usr_array['environment'],
                            /*New store app registration id*/
                            'store_id' =>$wp_usr_array['store_id'], 
                            /*New store app name*/
                            'store_name' =>$wp_usr_array['store_name'],
                            /*New store url.*/ 
                            'store_url' =>$wp_usr_array['store_url'], 
                            /*store app platform like: woocomerce, shopify etc.*/
                            'app_platform' =>$wp_usr_array['app_platform'], 
                            /*Get the app id*/
                            'app_id' =>$wp_usr_array['app_id'], 
                            /*Get the app name*/
                            'app_name' =>$wp_usr_array['app_name'], 
                            /*get app status is installed or uninstalled*/
                            'app_status' =>$wp_usr_array['app_status'], 
                            /*Registered email id*/
                            'email' =>$wp_usr_array['email'],
                            /*Registered user name*/ 
                            'name' =>$wp_usr_array['name'], 
                            'address' => $wp_usr_array['address'],
                            'phone' => $wp_usr_array['phone'],
                            'city' => $wp_usr_array['city'],
                            'country' => $wp_usr_array['country'],
                            'zip' => $wp_usr_array['zip'],
                            /*User's account get free or paid*/
                            'current_app_plan' =>$wp_usr_array['current_app_plan'],
                            'created_at' => date('Y-m-d H:i:s'),
                            'payment_status_update_at' => date('Y-m-d H:i:s'),
                            'app_status_update_at' => date('Y-m-d H:i:s')
                            );

                    if( $data['created_at'] ){
                        $NewUserData['created_at'] = $data['created_at'];
                    }

                   
                    if( $data['payment_status_update_at'] ){
                        $NewUserData['payment_status_update_at'] = $data['payment_status_update_at'];
                    }

                    
                    if($data['app_status_update_at']){
                        $NewUserData['app_status_update_at'] = $data['app_status_update_at'];
                    }

                    $wp_usr_query = $wpdb->insert( $tbname, $NewUserData );
                    
                    $dInsert = $wpdb->insert_id;
                    if($dInsert){
                        $response['status'] = 'success';
                        $response['msg'] = "App installtion has been done.";
                            return $response;
                    }else{
                        $response['status'] = 'false';
                        $response['msg'] = $wpdb->print_error();
                        return $response;
                    }
                }else{
                    $response['status'] = 'false';
                    $response['msg'] = "App installation not done due to technical issue.";
                    return $response;
                }
            }
        }
    }
    
    public function CheckIfEntryExist( $store_id, $app_id ){
        global $wpdb;
        $tbname = $wpdb->prefix.$this->tname;

        $sql = "SELECT store_id, app_id, app_platform FROM $tbname WHERE store_id = '$store_id' AND app_id = '$app_id'";
        $sqex = $wpdb->get_row( $sql );
       
        $app = array();
        if($sqex){
            $app['name'] = $sqex->app_platform;
            $app['status'] = true;
            return $app;
        }else{
            $app['status'] = false;
            return $app;
        }
    }

    public function update_status_mail_list( $email_list_update ){
        if( $email_list_update ) {
                /*$plat_id = badem_get_email_list_id( $args['app_platform'] );*/
                $list_ids = array();
                $ulist_ids = array();
                $list_ids[] = badem_get_email_list_id( $args['app_platform'] );
                $list_ids[] = badem_get_email_list_id( $args['app_platform'].'-active' );
                $list_ids[] = $args['app_mail_list_id'];
                
                $ulist_ids = badem_get_email_list_id( $args['app_platform'].'-inactive' );
                
                /*To add/subscribe clinet/users in mailPoet segment/list.*/
                badem_mailPoet_subscribe_to_list( $u, $list_ids );
                badem_mailPoet_unsubscribe_from_list( $u, $ulist_ids );
            }
        return true;
    }

    public function update_reinstallation_app( $environment = false, $Pform_name, $data_id, $data_name ){
        $rwdata = array(
                    'app_status'=> $data['app_status'],
                    'app_status_update_at' => date('Y-m-d H:i:s')
                    );
        $where = array(
                    'id'=> $udp_row_data->id
                    );
        $update_rows = $wpdb->update( $tbname, $rwdata, $where );
    }

    public function NewRowsadd( $environment = false, $Pform_name, $data_id, $data_name ){
        if( $Pform_name && $environment ){
            $CheckPlatform = $this->ExistPlatform( $Pform_name );
            switch ( $CheckPlatform['status'] ){
                case 1 :
                $this->ExistApps($environment, $data_id, $data_name, $CheckPlatform['id']);
                break;

                case 0 :
                $this->ExistApps($environment, $data_id, $data_name, $CheckPlatform['id']);
                break;
            }
            return true;
        }else{
            return false;
        }
    }

    public function ExistPlatform( $platform_name ){
        global $wpdb;
        $apTbl = $wpdb->prefix.'badem_platforms';
        $sql = "SELECT id, name FROM $apTbl WHERE slug = '$platform_name'";
        $sqex = $wpdb->get_row( $sql );
        $exPform = array();
        if( $sqex ){
            $exPform['id'] = $sqex->id;
            $exPform['status'] = true; 
            return $exPform;
        }else{
            $app_slug = str_replace( " ","-",$platform_name );
            
            switch($app_slug)
            {
                case 'big-commerce' :
                $platform_name = 'BigCommerce';
                break;

                case 'shopify' :
                $platform_name = 'Shopify';
                break;
            }
            
            $insertSql = array(
                            'name'  =>  $platform_name,
                            'slug'  =>  $app_slug  
                            );
            $wpdb->insert( $apTbl, $insertSql);
            $exPform['id'] = $wpdb->insert_id;
            $exPform['status'] = false;
            return $exPform;
        }
    }
    public function ExistApps( $environment, $app_id, $app_name, $plaform_id ){
        global $wpdb;
        $AppsTbl = $wpdb->prefix.'badem_apps';
        
        if( $plaform_id && $environment ){
            switch( $environment ){
                case '1' :
                $environments = '0';
                break;

                case '2' :
                $environments = '1';
                break;
            }
            $sql = "SELECT app_name FROM $AppsTbl WHERE environment = '$environments' AND app_name = '$app_name' and app_id = '$app_id'";
            $sqex = $wpdb->get_row( $sql );
            if( $sqex ){
                return true;
            }else{
                $inserApps = array(
                                'environment' => $environments,
                                'app_id'    =>  $app_id,
                                'app_name'  =>  $app_name,
                                'platform'  =>  $plaform_id
                                );
                $Apps_insert = $wpdb->insert( $AppsTbl, $inserApps);
                $wpdb->insert_id;
            }
        }
    }
    public function ValidateData($data) {

        $wp_usr_array = array();
        $response = array();

        if( intval( $data['environment'] ) == 1 || intval( $data['environment'] ) == 2 ){
            if( intval($data['environment']) == 1){
            $wp_usr_array['environment'] = '0';
            }else{
                $wp_usr_array['environment'] = '1';
            }
        }else{
            $response['status'] = 'false';
            $response['msg'] = "Environment not defined for access data.";
        }
        /* New store app registration id / Hash keys */
        if( sanitize_text_field( $data['store_id'] ) ){
             $wp_usr_array['store_id'] = $data['store_id'];
        }else{
            $response['status'] = 'false';
            $response['msg'] = "Please reinsert correct store id.";
        }
        /* New store app Url from store Hash keys 
        if( preg_match( "/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",$data['store_url'] ) )*/
        if( $data['store_url'] )
        {
            $store_url = $data['store_url'];
            $wp_usr_array['store_url'] = "$store_url";
        }else{
                $response['status'] = 'false';
                $response['msg'] = "Please check it, fill correct link.";
            }

        /* New store app name from store Hash keys */
        if( $data['store_name'] ){
            $store_name = $data['store_name'];
            $wp_usr_array['store_name'] = "$store_name";
        }else{
            $response['status'] = 'false';
            $response['msg'] = "Please fill required store name field.";
        }

        /* store app belonging platform like: woocomerce, shopify, bigcommerce etc. */
        if( $data['app_platform'] == "big-commerce" || $data['app_platform'] == "shopify" ){
            $wp_usr_array['app_platform'] = $data['app_platform'];
        }else{
            $response['status'] = 'false';
            $response['msg'] = "This platform not exist in our data.";
        }

        /* Get the app id from app hash key */
        if( sanitize_text_field ( $data['app_id'] ) ){
            $wp_usr_array['app_id'] = $data['app_id'];
        }else{
            $response['status'] = 'false';
            $response['msg'] ="Please provide correct app id.";
        }

        /* Get the app name from hash key */
        if( sanitize_text_field ( $data['app_name'] ) ){
            $wp_usr_array['app_name'] = $data['app_name'];
        }else{
            $response['status'] = 'false';
            $response['msg'] = "Please fill required app name.";
        }

        /* get app status is active or inactive */
        if( $data['app_status'] == "active" ){
            $wp_usr_array['app_status'] = $data['app_status'];
        }else{
            $response['status'] = 'false';
            $response['msg'] = "App installtion status is blank.";
        }

        /* Registered email id getting from app or store */
        if( $data['email'] ){
            $email = $data['email'];
                $wp_usr_array['email'] = "$email";
            }else{
                $response['status'] = 'false';
                $response['msg'][] = "Please fill required email id.";

            }

        /* Registered user name getting from app or store */
        $wp_usr_array['name'] = 'NA';
        if( $data['name'] ){
            $name = $data['name'];
            $wp_usr_array['name'] = "$name";
        }
        /*else{
            $response['status'] = 'false';
            $response['msg'] = "Please fill required user full name.";
        }*/

        /* Registered user address */
        $wp_usr_array['address'] = 'NA';
        if( $data['address'] ){
            $address = $data['address'];
            $wp_usr_array['address'] = "address";
        }
        /*else{
            $response['status'] = 'false';
            $response['msg'] = "Please fill required registered store address.";
        }*/

        /* Registered user phone */
        if( $data['phone'] ){
            $wp_usr_array['phone'] = $data['phone'];
        }else{
            $response['status'] = 'false';
            $response['msg'] = "Please fill required registered contact no.";

        }

        /* Registered user city*/
        $wp_usr_array['city'] = 'NA';
        if( $data['city'] ){
            $city = $data['city'];
            $wp_usr_array['city'] = "$city";
        }
        /*else{
            $response['status'] = 'false';
            $response['msg'] = "Please fill required city name.";
        }*/

        /* Registered user country */
        if( $data['country'] ){
            $country = $data['country'];
            $wp_usr_array['country'] = "$country";
        }else{
            $response['status'] = 'false';
            $response['msg'] = "Please fill required country name.";

        }

        /* Registered user zip code */
        $wp_usr_array['zip'] = 'NA';
        if( $data['zip'] )
        {
            $wp_usr_array['zip'] = $data['zip'];
        }
        /*else{
            $response['status'] = 'false';
            $response['msg'] = "Please fill required area zip.";

        }*/

        /* User's account get free or paid */
        if( $data['current_app_plan'] == "free" || $data['current_app_plan'] == "paid" || $data['current_app_plan'] == "trial" )
        {
            $wp_usr_array['current_app_plan'] = $data['current_app_plan'];
        }else{
            $response['status'] = 'false';
            $response['msg'] = "Your subscription status is blank.";

        }
       

        if($response){
            return $response;
        }else{
            return $wp_usr_array;
        }
    }
}
new app_installation_by_user;
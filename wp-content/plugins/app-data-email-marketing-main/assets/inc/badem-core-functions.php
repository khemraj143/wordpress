<?php 

/*****
    Description: Ajax handlers file for handling js code without page refresh.
    Author: BitCot
    Created: 30-Mar-2021
    Modify: 06-Apr-2021

*****/
/*If this file is called directly, abort.*/
if ( ! defined( 'WPINC' ) ) {die;} 

/*
 * Mailpoet Plugin Changes
 */

function badem_NameConversion($Names){

    if($Names == 'big-commerce' || $Names == 'shopify'){
        $ConName = array();
        switch($Names){
            case 'big-commerce' :
            $ConName['big-commerce'] = 'BigCommerce';
            break;

            case 'shopify' :
            $ConName['shopify'] = 'Shopify';
            break;
        }
        return $ConName;
    }
}

function badem_get_all_mailPoet_Lists(){
    if (class_exists(\MailPoet\API\API::class)) {
        $mailpoet_api = \MailPoet\API\API::MP('v1');    
        return $mailpoet_api->getLists();
    }
    return false;
}

    /******* dynamic Email list creating *******/

function badem_create_get_list_id($spform = '', $sappid = '', $sapps = '', $apstatus = ''){
    $inserts = ''; 
    if (class_exists(\MailPoet\API\API::class)) {
        $mailpoet_api = \MailPoet\API\API::MP('v1');   
        $elist = validate_email_date($spform, $sappid, $sapps, $apstatus);
        $inserted = array();

        if($elist){
            for ($el=0; $el < count($elist) ; $el++) {
                $desc = $elist[$el].' All users list';
            
                $list = array(
                            'name' => $elist[$el],
                            'description' => $desc
                            );
                $inserted[] = $mailpoet_api->addList($list);
            } 
            $inserts = array_merge($inserted);   
        }
            
    }
    return $inserts;
}

function filter_request_by_datatable ( $table, $data )
{
    $table1_term = 't1';
    $table2_term = 't2';

    $set_platform = '';
    if( $data['fltr_platform'] )
    {   
        $fltr_platform = $data['fltr_platform'];
        if( count($table) > 1 )
        {
            $set_platform = "AND $table1_term.app_platform = '$fltr_platform'";
        }else
        {
            $set_platform = "AND app_platform = '$fltr_platform'";
        }
    }

    $set_app = '';
    if( $data['fltr_app'] )
    {
        $fltr_app = $data['fltr_app'];
        if( count($table) > 1 )
        {
            $set_app = "AND $table1_term.app_id = '$fltr_app'";
        }else
        {
            $set_app = "AND app_id = '$fltr_app'";
        }
    }

    $set_app_status = '';
    if( $data['fltr_app_status'] )
    {
        $fltr_app_status = $data['fltr_app_status'];
        if( count($table) > 1 )
        {
            $set_app_status = "AND $table1_term.app_status = '$fltr_app_status'";
        }else
        {
            $set_app_status = "AND app_status = '$fltr_app_status'";
        }
    }

    $set_app_type = '';
    if( $data['fltr_app_type'] )
    {
        $fltr_app_type = $data['fltr_app_type'];
        if( count($table) > 1 )
        {
            $set_app_type = "AND $table1_term.current_app_plan = '$fltr_app_type'";
        }else
        {
            $set_app_type = "AND current_app_plan = '$fltr_app_type'";
        }
    }

    $set_pay_status = '';
    if( $data['fltr_pay_status'] )
    {
        $fltr_pay_status = $data['fltr_pay_status'];
        if( count($table) > 1 )
        {
            $set_pay_status = "AND $table2_term.payment_status = '$fltr_pay_status'";
        }else
        {
            $set_pay_status = "AND payment_status = '$fltr_pay_status'";
        }
    }

    $set_from_date = '';
    if( $data['fltr_from_date'] )
    {
        $fltr_from_date = $data['fltr_from_date'];
        if( count($table) > 1 )
        {
            $set_from_date = "AND DATE($table2_term.created_at) >= '$fltr_from_date'";
        }else
        {
            $set_from_date = "AND DATE(created_at) >= '$fltr_from_date'";
        }
    }

    $set_to_date = '';
    if( $data['fltr_to_date'] )
    {
        $fltr_to_date = $data['fltr_to_date'];
        if( count($table) > 1 )
        {
            $set_to_date = "AND DATE($table2_term.created_at) <= '$fltr_to_date'";
        }else
        {
            $set_to_date = "AND DATE(created_at) <= '$fltr_to_date'";
        }
    }

    return array(
                'platform' => $set_platform,
                'app' => $set_app,
                'app_status' => $set_app_status,
                'app_type' => $set_app_type,
                'pay_status' => $set_pay_status,
                'from_date' => $set_from_date,
                'to_date' => $set_to_date
                );
}

function common_request_by_datatable( $data )
{
    /*** For get column name ***/
        $table_columns = array();
        if( $data['columns'] )
        {
            for ($p=0; $p < count($data['columns']); $p++)
            { 
                $table_columns[] = $data['columns'][$p]['data'];
            }
        }
        
        /*** For Order by query ***/
        if( $data['order'][0]['dir'] )
        {
            $order = $data['order'][0]['column'];
            $order_asc = $data['order'][0]['dir'];

            if( $order > 0 )
            {
                $o_start = $table_columns[$order];
                $order_query = "ORDER BY ".$o_start.' '.$order_asc;
            }else
            {
                $o_start = $table_columns[$order];
                $order_query = "ORDER BY ".$o_start.' '.$order_asc;
            }

        }

        /*** For Search query ***/
        $search_query = '';
        if( $data['search']['value'] )
        {
            $search = trim( $data['search']['value'] );
            $col_search = array();
            for ($b=0; $b < count($data['columns']); $b++)
            {
                $searchable = $data['columns'][$b]['searchable'];
                if( $searchable == 'true' )
                {
                    $col_search[] = $table_columns[$b]." LIKE '%$search%'";
                }
            }

            $strn = implode(" ", $col_search);
            $search_query = str_replace("' ", "' OR ", $strn);
        }
        

        /*** For Limit query ***/
        $start = $data['start'];
        $end = $data['length'];
        if( $data['start'] == 0 && $data['length'] > 0 )
        {
           $limit_query = "LIMIT $start , $end";
        }else
        {
            $limit_query = "LIMIT $start , $end";
        }
        return array(
                    'table_columns' => $table_columns,
                    'order_query' => $order_query,
                    'searches' => $search_query,
                    'limit_query' => $limit_query
                    );
}

function app_datatable_multitable_callback( $table, $data )
{
    global $wpdb;
    if( $data )
    {
        if( count($table) > 1 )
        {
            $table1 = $wpdb->prefix.$table[0];
            $table2 = $wpdb->prefix.$table[1];
            $table1_term = 't1';
            $table2_term = 't2';
            $table_name = array(
                            'table1' => $wpdb->prefix.$table[0],
                            'table2' => $wpdb->prefix.$table[1],
                            'table1_term' => $table1_term,
                            'table2_term' => $table2_term
                            );
        }else
        {
            $table_name = $table;
        }

        $e_id = get_option( 'data_access_mode' );

        $sql_request = common_request_by_datatable( $data );

        $serach = '';
        if( $sql_request['searches'] )
        {
            $searches = $sql_request['searches'];
            $serach = "AND ( ". $searches ." )";
        }
        
        $order_query = '';
        if( $sql_request['order_query'] )
        {
            $order_query = $sql_request['order_query'];
        }
        
        $limit_query = '';
        if( $sql_request['limit_query'] )
        {
            $limit_query = $sql_request['limit_query'];
        }
        
        $table_columns = '';
        if( $sql_request['table_columns'] )
        {
            $table_columns = $sql_request['table_columns'];
        }

        $filter_request = filter_request_by_datatable( $table_name, $data );

        $fltr_platform = '';
        if( $filter_request['platform'] )
        {
            $fltr_platform = $filter_request['platform'];
        }

        $fltr_app = '';
        if( $filter_request['app'] )
        {
            $fltr_app = $filter_request['app'];
        }

        $fltr_app_status = '';
        if( $filter_request['app_status'] )
        {
            $fltr_app_status = $filter_request['app_status'];
        }

        $fltr_app_type = '';
        if( $filter_request['app_type'] )
        {
            $fltr_app_type = $filter_request['app_type'];
        }

        $fltr_pay_status = '';
        if( $filter_request['pay_status'] )
        {
            $fltr_pay_status = $filter_request['pay_status'];
        }

        $fltr_from_date = '';
        if( $filter_request['from_date'] )
        {
            $fltr_from_date = $filter_request['from_date'];
        }

        $fltr_to_date = '';
        if( $filter_request['to_date'] )
        {
            $fltr_to_date = $filter_request['to_date'];
        }

        if( $table )
        {
            $sql_column_query = "SHOW COLUMNS FROM wp_hubify.$table2";
            $table_column = $wpdb->get_results($sql_column_query);
            $cols = array();
            foreach ($table_column as $key => $value) {
                $cols[] = $value->Field;
            }
            
        }

        $table_cols = array();
        for ($tc=0; $tc < count($cols); $tc++) { 
            $table_cols[] = $table2_term.".".$cols[$tc];
        }
        array_push($table_cols,"$table1_term.app_name","$table1_term.name","$table1_term.email");
        $table_cols_text = implode(" ", $table_cols);
        $table_cols_wrt = str_replace(" "," , ",$table_cols_text);

        $sql_multi_table_query = "SELECT $table_cols_wrt FROM $table2 $table2_term LEFT JOIN $table1 $table1_term ON $table2_term.store_id = $table1_term.id WHERE $table1_term.environment = '$e_id' $serach $fltr_platform $fltr_app $fltr_app_status $fltr_app_type $fltr_pay_status $fltr_from_date $fltr_to_date $order_query $limit_query";
        
        $sql_multi_table_total_query = "SELECT $table_cols_wrt FROM $table2 $table2_term LEFT JOIN $table1 $table1_term ON $table2_term.store_id = $table1_term.id WHERE $table1_term.environment = '$e_id'  $serach $fltr_platform $fltr_app $fltr_app_status $fltr_app_type $fltr_pay_status $fltr_from_date $fltr_to_date $serach";

        $table_data = $wpdb->get_results($sql_multi_table_query);
        $table_total_data = $wpdb->get_results($sql_multi_table_total_query);   

    }
    return array(
                'dir_request' => $sql_request,
                'flr_request' => $filter_request,
                'results' => $table_data,
                'total' => count($table_total_data),
                'queries' => $sql_multi_table_query,
                );
}


function app_datatable_calback( $table, $data )
{
    global $wpdb;

    $e_id = get_option( 'data_access_mode' );

    if( $data )
    {
        if( count($table) > 1 )
        {
            $table1 = $wpdb->prefix.$table[0];
            $table2 = $wpdb->prefix.$table[1];
            $table1_term = 't1';
            $table2_term = 't2';
            $table_name = array(
                            'table1' => $wpdb->prefix.$table[0],
                            'table2' => $wpdb->prefix.$table[1],
                            'table1_term' => $table1_term,
                            'table2_term' => $table2_term
                            );
        }else
        {
            $table_name = $table;
        }
        

        $data_env = '';
        if( $table_name == 'app_users')
        {
            $data_env = "WHERE environment = '$e_id'";
        }

        $sql_request = common_request_by_datatable( $data );

        $search = '';
        if( $sql_request['searches'] )
        {
            switch( $table )
            {
                case 'apps_platform' :
                $searches = $sql_request['searches'];
                $search = "WHERE ( $searches )";
                break;

                case 'app_users' :
                $searches = $sql_request['searches'];
                $search = "AND ( $searches )";
                break;
            }
        }
        
        $order_query = '';
        if( $sql_request['order_query'] )
        {
            $order_query = $sql_request['order_query'];
        }
        
        $limit_query = '';
        if( $sql_request['limit_query'] )
        {
            $limit_query = $sql_request['limit_query'];
        }

        $filter_request = filter_request_by_datatable( $table_name, $data );

        $fltr_platform = '';
        if( $filter_request['platform'] )
        {
            $fltr_platform = $filter_request['platform'];
        }

        $fltr_app = '';
        if( $filter_request['app'] )
        {
            $fltr_app = $filter_request['app'];
        }

        $fltr_app_status = '';
        if( $filter_request['app_status'] )
        {
            $fltr_app_status = $filter_request['app_status'];
        }

        $fltr_app_type = '';
        if( $filter_request['app_type'] )
        {
            $fltr_app_type = $filter_request['app_type'];
        }

        $fltr_pay_status = '';
        if( $filter_request['pay_status'] )
        {
            $fltr_pay_status = $filter_request['pay_status'];
        }

        $fltr_from_date = '';
        if( $filter_request['from_date'] )
        {
            $fltr_from_date = $filter_request['from_date'];
        }

        $fltr_to_date = '';
        if( $filter_request['to_date'] )
        {
            $fltr_to_date = $filter_request['to_date'];
        }

        /*** For get database data ***/
        if( $table )
        {
            $sql_query = "SELECT * FROM {$wpdb->prefix}$table $data_env $search $fltr_platform $fltr_app $fltr_app_status $fltr_app_type $fltr_from_date $fltr_to_date $order_query $limit_query";

            $sql = "SELECT * FROM {$wpdb->prefix}$table $data_env $fltr_platform $fltr_app $fltr_app_status $fltr_app_type $fltr_from_date $fltr_to_date $search";
        }

        $sql_results = $wpdb->get_results( $sql_query );
        $sql_total = $wpdb->get_results( $sql );
    }
    return array(
                'dir_request' => $sql_request,
                'flr_request' => $filter_request,
                'results' => $sql_results,
                'total' => count($sql_total),
                'queries' => $sql_query,
                );
}

/* Graph data generate based on DB data */
function get_data_by_sql( $req, $id )
{
    $enid = get_option( 'data_access_mode' );
    global $wpdb;

    if( $id == '#stat_graph' )
    {

        if( $req )
        {
            $current = date( 'Y-m-d' );

            $colunm = "";
            if( $req['colunm'] )
            {
                $colunm = $req['colunm'];
            }

            $group = "";
            if( $req['group'] )
            {
                $group = $req['group'];
            }

            $to_year = '';
            if( $req['fltr_year'] )
            {
                $last_year = $req['fltr_year'];
                $to_year = "AND date( pt.start_at ) <= '$current' AND date( pt.start_at ) >= '$last_year'";
            }

            $to_half = '';
            if( $req['fltr_half_year'] )
            {
                $fltr_half_year = $req['fltr_half_year'];
                $to_half = "AND date( pt.start_at ) <= '$current' AND date( pt.start_at ) >= '$fltr_half_year'";
            }

            $to_quart = '';
            if( $req['fltr_quarter'] )
            {
                $fltr_quarter = $req['fltr_quarter'];
                $to_quart = "AND date( pt.start_at ) <= '$current' AND date( pt.start_at ) >= '$fltr_quarter'";
            }

            $to_week = '';
            if( $req['fltr_week'] )
            {
                $fltr_first_day = $req['fltr_week'];
                $to_week = "AND date( pt.start_at ) <= '$current' AND date( pt.start_at ) >= '$fltr_first_day'";
            }

            $from_query = '';
            if( $req['from_filter'] && !$req['to_filter'] )
            {
                $from_filter = $req['from_filter'];
                $from_query = "AND date( pt.start_at ) >= '$from_filter'";
            }

            $to_query = '';
            if( $req['to_filter'] && !$req['from_filter'] )
            {
                $to_filter = $req['to_filter'];
                $to_query = "AND date( pt.start_at ) <= '$to_filter'";
            }

            $bw_query = '';
            if( $req['from_filter'] && $req['to_filter'] )
            {
                $from_filter = $req['from_filter'];
                $to_filter = $req['to_filter'];
                $bw_query = "AND date( pt.start_at ) >= '$from_filter' AND date( pt.start_at ) <= '$to_filter'";
            }

        }

        $graph_query = "SELECT $colunm FROM {$wpdb->prefix}badem_payments pt LEFT JOIN {$wpdb->prefix}app_users ur ON pt.store_id = ur.id WHERE ur.environment = '$enid' AND pt.payment_status = 'paid' $to_year $to_half $to_quart $to_week $from_query $to_query $bw_query $group";
    }

    if( $id == '#user_graph' )
    {
        if( $req )
        {
            $current = date( 'Y-m-d' );

            $colunm = "";
            if( $req['colunm'] )
            {
                $colunm = $req['colunm'];
            }

            $group = "";
            if( $req['group'] )
            {
                $group = $req['group'];
            }

            $to_year = '';
            if( $req['fltr_year'] )
            {
                $last_year = $req['fltr_year'];
                $to_year = "AND date( created_at ) <= '$current' AND date( created_at ) >= '$last_year'";
            }

            $to_half = '';
            if( $req['fltr_half_year'] )
            {
                $fltr_half_year = $req['fltr_half_year'];
                $to_half = "AND date( created_at ) <= '$current' AND date( created_at ) >= '$fltr_half_year'";
            }

            $to_quart = '';
            if( $req['fltr_quarter'] )
            {
                $fltr_quarter = $req['fltr_quarter'];
                $to_quart = "AND date( created_at ) <= '$current' AND date( created_at ) >= '$fltr_quarter'";
            }

            $to_week = '';
            if( $req['fltr_week'] )
            {
                $fltr_first_day = $req['fltr_week'];
                $to_week = "AND date( created_at ) <= '$current' AND date( created_at ) >= '$fltr_first_day'";
            }

            $from_query = '';
            if( $req['from_filter'] && !$req['to_filter'] )
            {
                $from_filter = $req['from_filter'];
                $from_query = "AND date( created_at ) >= '$from_filter'";
            }

            $to_query = '';
            if( $req['to_filter'] && !$req['from_filter'] )
            {
                $to_filter = $req['to_filter'];
                $to_query = "AND date( created_at ) <= '$to_filter'";
            }

            $bw_query = '';
            if( $req['from_filter'] && $req['to_filter'] )
            {
                $from_filter = $req['from_filter'];
                $to_filter = $req['to_filter'];
                $bw_query = "AND date( created_at ) >= '$from_filter' AND date( created_at ) <= '$to_filter'";
            }

        }
        
        $graph_query = "SELECT $colunm FROM {$wpdb->prefix}app_users WHERE environment = '$enid' AND app_status = 'active' $to_year $to_half $to_quart $to_week $from_query $to_query $bw_query $group";
    }

    $results = $wpdb->get_results( $graph_query );

        if( $results )
        {
            return array(
                    'rid' => $id,
                    'data' => $results,
                    'sql_query' => $graph_query,
                    );
        }
    }

    

function selection_platform_list()
{
    ob_start();
    global $wpdb;
    
    $platform_list_sql = "SELECT t1.name, t1.slug FROM {$wpdb->prefix}badem_platforms t1 LEFT JOIN {$wpdb->prefix}app_users t2 ON t1.slug = t2.app_platform GROUP BY name";
    $get_platform_list = $wpdb->get_results($platform_list_sql);
    
    if($get_platform_list)
    {
        foreach ($get_platform_list as $key => $value) {
        ?>
            <option value="<?php echo $value->slug; ?>"> <?php echo $value->name; ?> </option>
        <?php
        }
    }else
    {
        return "No platform list in our data.";
    }
    return ob_get_clean();
}

function selection_app_lists( $filter_apps )
{
    ob_start();
    global $wpdb;

    $e_id = get_option( 'data_access_mode' );

    $filter_by_platform = '';
    if( $filter_apps['fltr_platform'] == 'big-commerce' || $filter_apps['fltr_platform'] == 'shopify' )
    {
        $platform = $filter_apps['fltr_platform'];
        $filter_by_platform = "AND t2.slug = '$platform'";
    }

    $app_id = '';
    if( $filter_apps['fltr_app'] )
    {
        $app_id = $filter_apps['fltr_app'];
    }

    $app_list_sql = "SELECT t1.app_id, t1.app_name, t2.slug, t2.name as platform FROM {$wpdb->prefix}badem_apps t1 LEFT JOIN {$wpdb->prefix}badem_platforms t2 ON t1.platform = t2.id LEFT JOIN {$wpdb->prefix}app_users t3 on t2.slug = t3.app_platform WHERE t3.environment = '$e_id' $filter_by_platform GROUP BY t1.app_id, t1.app_name, t2.slug, t2.name ";

    /*$app_list_sql = "SELECT t1.app_id, t1.app_name, t2.name as platform FROM {$wpdb->prefix}badem_apps t1 LEFT JOIN {$wpdb->prefix}badem_platforms t2 ON t1.platform = t2.id LEFT JOIN {$wpdb->prefix}app_users t3 ON t1.app_id = t3.app_id WHERE t3.environment = '$e_id' $filter_by_platform $filter_by_app ";*/

    $app_lists = $wpdb->get_results($app_list_sql);

    if( $app_lists )
    {
        ?>
        <select name="apps" class="select_app" id="app_list" >
            <option value=""> All Apps </option>
            <?php
        foreach ($app_lists as $key => $value)
        {
        ?>
            <option value="<?php echo $value->app_id; ?>" <?php if($value->app_id == $app_id){echo 'selected';}?> > <?php echo $value->app_name; ?> </option>
            
        <?php
        }
        ?>
        </select>
        <?php
    }else
    {
        ?>
        <select name="apps" class="select_app" id="app_list" >
            <option value=""> Blank App List </option>
        </select>
        <?php
    }
    $content = ob_get_clean();

    return array(
                'get_data' => $app_lists,
                'sql' => $app_list_sql,
                'content' => $content,
                'app_id' => $filter_apps['fltr_app']
                );
}

function get_appid_by_platform( $platform, $app_name )
{
    if( $platform == 'shopify' || $platform == 'big-commerce' )
    {
        if( $platform && $app_name )
        {
            global $wpdb;
            $appid_sql = "SELECT * FROM {$wpdb->prefix}apps_platform WHERE app_name = '$app_name' AND platform = '$platform'";
            return $wpdb->get_row( $appid_sql );
        }    
    }
}


function create_nonce_at(...$nonce_data)
{
    if($nonce_data){
        $exp_url = admin_url().$nonce_data[0];
        $exp_data = add_query_arg( 
                        array( 
                            'action' => $nonce_data[1],
                        ), 
                        $exp_url
                    );
    }
    return wp_nonce_url( $exp_data, $nonce_data[2], $nonce_data[3] );
}

function update_email_list($splat, $sappid, $sapps, $apstatus){
    $nlname = badem_NameConversion($splat);
    $acmail_list = badem_get_email_list_id( $nlname[$splat].'-'.$sapps.'-active' );
    $plmail_list = badem_get_email_list_id( $nlname[$splat].'-'.$sapps );
    $up_list = array();
    if($acmail_list && $plmail_list){
        
        if($apstatus == 'inactive'){
            $pname = badem_NameConversion($splat);
            $up_list[] = $pname[$splat].'-'.$sapps.'-'.$apstatus; 
        }
    }else{
        $nlname = badem_NameConversion($splat);
        $up_list[] = $nlname[$splat].'-'.$sapps.'-active';
        $up_list[] = $nlname[$splat].'-'.$sapps;
    }
    
return $up_list;
}

function validate_email_date($splat, $sappid, $sapps, $apstatus){
    
    if($splat && $sappid && $sapps && $apstatus){
        global $wpdb;
        $list_user = $wpdb->prefix.'app_users';
        $match_query = $wpdb->get_row("SELECT app_platform, app_id, app_name, app_status FROM $list_user WHERE app_platform = '$splat' AND app_id = '$sappid' AND app_name = '$sapps' ");

        $array_list =array();
        if(!$match_query){
            if($apstatus == 'active'){
                $pname = badem_NameConversion($splat);
                $array_list[] = $pname[$splat].'-'.$sapps;
                $array_list[] = $pname[$splat].'-'.$sapps.'-'.$apstatus;    
            }
        }else{

            return update_email_list($splat, $sappid, $sapps, $apstatus);
        }

    return $array_list;
    }
}


function badem_get_email_list_id($slug){

    if (class_exists(\MailPoet\API\API::class)) {
        $mailpoet_api = \MailPoet\API\API::MP('v1'); 
        $getList = $mailpoet_api->getLists();

        foreach ($getList as $gkey => $evalue) {
            $eid = '';
            if($evalue['name'] == $slug){
                return $evalue['id'];
            }
        }
    }

}

add_action('init', 'badem_set_custom_global_variable' );
function badem_set_custom_global_variable()
{
    global $badem_platforms, $wpdb;
    /*$dataModes = get_option( 'data_access_mode' );
    $apps_table = $wpdb->prefix.'badem_apps';*/
    $platforms_table = $wpdb->prefix.'badem_platforms';
    $platforms = $wpdb->get_results("SELECT * FROM $platforms_table");
    $badem_platforms = array();
    if( $platforms ){
        foreach ($platforms as $key => $pt) {
            $badem_platforms[$pt->slug] = $pt->name;
        }
    }

}

function badem_mailPoet_subscribe_to_list($user, $list_ids){
    $option = array('send_confirmation_email' => false, 'schedule_welcome_email' => false);
    if (class_exists(\MailPoet\API\API::class)) {
        $mailpoet_api = \MailPoet\API\API::MP('v1');            
        $get_subscriber = '';
        /*Get user details*/
        if (!$user) {
            return;
        }

        $email          = $user['email'];
        $first_name     = $user['fname'];
        $last_name      = $user['lname'];

        /*Check if subscriber exists. If subscriber doesn't exist an exception is thrown*/
        try {
            $get_subscriber = $mailpoet_api->getSubscriber($email);
        } catch (\Exception $e) {}

        try {
            if (!$get_subscriber) {
              /*Subscriber doesn't exist let's create one*/
              $mel_list = $mailpoet_api->addSubscriber([
                                            'email' => $email,
                                            'first_name' => $first_name,
                                            'last_name' => $last_name,
                                          ], $list_ids, $option);
            } else {
              /*In case subscriber exists just add him to new lists*/
              $mailpoet_api->subscribeToLists($email, $list_ids, $option);
            }
        } catch (\Exception $e) {
            return $error_message = $e->getMessage(); 
        }
        badem_mailPoet_confirme_subscriber($email);
    }
    return true;
}

function badem_mailPoet_unsubscribe_from_list($user, $list_ids){
    if (class_exists(\MailPoet\API\API::class)) {
        $mailpoet_api = \MailPoet\API\API::MP('v1');            
        $get_subscriber = '';
        /*Get user details*/
        if (!$user) {
            return;
        }

        $email = $user['email'];
        
        /*Check if subscriber exists. If subscriber doesn't exist an exception is thrown*/
        try {
            $get_subscriber = $mailpoet_api->getSubscriber($email);
        } catch (\Exception $e) {
            return $error_message = $e->getMessage();
        }

        try {
            if ($get_subscriber) {
              $mailpoet_api->unsubscribeFromLists($email, $list_ids);

            }
        } catch (\Exception $e) {
            return $error_message = $e->getMessage(); 
        }
    }

    return true;
}

function badem_mailPoet_confirme_subscriber($email){
    global $wpdb;
    $table = $wpdb->prefix. 'mailpoet_subscribers';
    $update = array('status'=>'subscribed');
    $where = array('email'=> $email);

    if( $wpdb->get_row("SELECT * FROM $table WHERE email = '$email'") ){
        $wpdb->update($table, $update, $where);
    }
}


function sql_callback_for_dashboard_widget( $table, $query)
{
    global $wpdb;
    $envir = get_option( 'data_access_mode' );

    $platform = '';
    if( $query['filter']['platform'] )
    {
        $platform = $query['filter']['platform'];
    }

    $app = '';
    if( $query['filter']['app'] )
    {
        $app = $query['filter']['app'];
    }

    $from_date = '';
    if( $query['filter']['from_date'] )
    {
        $from_date = $query['filter']['from_date'];
    }

    $to_date = '';
    if( $query['filter']['to_date'] )
    {
        $to_date = $query['filter']['to_date'];
    }

    if( count($table) > 1 )
        {
            $table1 = $table[0];
            $table2 = $table[1];
            $table1_term = 't1';
            $table2_term = 't2';
            
            $revenue = '';
            if( $query['revenue'] )
            {
                $revenue = $query['revenue'];
            }

            $column = '';
            if( $query )
            {
                $column = $query['column'];
            }

            $month = '';
            if( $query['month'] )
            {
                $month = $query['month'];   
            }

            $year = '';
            if( $query['year'] )
            {
                $year = $query['year']; 
            }

            $sql_query = "SELECT $column FROM {$wpdb->prefix}$table2 $table2_term LEFT JOIN {$wpdb->prefix}$table1 $table1_term ON $table2_term.store_id = $table1_term.id WHERE $table1_term.environment = '$envir' $revenue $month $year $platform $app $from_date $to_date";

        }else
        {
            $table_name = $table;

            $column = '';
            if( $query['column'] )
            {
                $column = $query['column'];
            }

            $limit = '';
            if( $query['limit'] )
            {
                $limit = $query['limit'];
            }

            $ordered = '';
            if( $query['order'] )
            {
                $ordered = $query['order'];
            }

            $status = '';
            if( $query['status'] )
            {
                $status = $query['status']; 
            }

            $month = '';
            if( $query['month'] )
            {
                $month = $query['month'];   
            }

            $year = '';
            if( $query['year'] )
            {
                $year = $query['year']; 
            }

            $sql_query = "SELECT $column FROM {$wpdb->prefix}$table_name WHERE environment = '$envir' $month $year $ordered $limit $platform $app $from_date $to_date $status";
        }

    
    $db_data = $wpdb->get_results( $sql_query );

    return array(
                'results' => $db_data,
                'query' => $sql_query
                );
}

function dashboard_widget_data( $data )
{
    global $wpdb;
    $envir = get_option( 'data_access_mode' );
    $table = array( 'app_users', 'badem_payments');

    if( $data )
    {
        $revenue = '';
        if( $data['revenue'] )
        {
            $revenue = $data['revenue'];
        }

        $limit = '';
        if( $data['length'] )
        {
            $start = $data['start'];
            $length = $data['length'];
            $limit = "LIMIT $start, $length";
        }

        $ordered = "ORDER BY created_at DESC";

        $req_query = array( 'column' => '*',
                            'limit' => $limit,
                            'order' => $ordered
                            );         
        
        $act_month_cnt = array(
                    'column' => "COUNT(created_at) as month",
                    'month' => "AND MONTH(created_at) = MONTH(CURDATE())",
                    'year' => "AND YEAR(created_at) = YEAR(CURDATE())",
                    'status' => "AND app_status = 'active'",
                    'filter' => filter_request_by_datatable( $table[0] , $data )
                    );
        $act_year_cnt = array(
                    'column' => "COUNT(created_at) as year",
                    'year' => "AND YEAR(created_at) = YEAR(CURDATE())",
                    'status' => "AND app_status = 'active'",
                    'filter' => filter_request_by_datatable( $table[0] , $data )
                    );

        $act_total_cnt = array(
                    'column' => "COUNT(created_at) as total",
                    'status' => "AND app_status = 'active'",
                    'filter' => filter_request_by_datatable( $table[0] , $data )
                    );

        $in_month_cnt = array(
                    'column' => "COUNT(created_at) as month",
                    'month' => "AND MONTH(created_at) = MONTH(CURDATE())",
                    'year' => "AND YEAR(created_at) = YEAR(CURDATE())",
                    'status' => "AND app_status = 'inactive'",
                    'filter' => filter_request_by_datatable( $table[0] , $data )
                    );
        $in_year_cnt = array(
                    'column' => "COUNT(created_at) as year",
                    'year' => "AND YEAR(created_at) = YEAR(CURDATE())",
                    'status' => "AND app_status = 'inactive'",
                    'filter' => filter_request_by_datatable( $table[0] , $data )
                    );

        $in_total_cnt = array(
                    'column' => "COUNT(created_at) as total",
                    'status' => "AND app_status = 'inactive'",
                    'filter' => filter_request_by_datatable( $table[0] , $data )
                    );

        $month_rev = array(
                        'column' => "FORMAT(SUM(t2.amount), 2) as month",
                        'month' => "AND MONTH(t2.start_at) = MONTH(CURDATE())",
                        'year' => "AND YEAR(t2.start_at) = YEAR(CURDATE())",
                        'revenue' => "AND t2.payment_status = '$revenue'",
                        'filter' => filter_request_by_datatable( $table , $data )
                        );

        $year_rev = array(
                        'column' => "FORMAT(SUM(t2.amount), 2) as year",
                        'year' => "AND YEAR(t2.start_at) = YEAR(CURDATE())",
                        'revenue' => "AND t2.payment_status = '$revenue'",
                        'filter' => filter_request_by_datatable( $table , $data )
                        );

        $total_rev = array(
                        'column' => "FORMAT(SUM(t2.amount), 2) as total",
                        'revenue' => "AND t2.payment_status = '$revenue'",
                        'filter' => filter_request_by_datatable( $table , $data )
                        );

        $recent_app = sql_callback_for_dashboard_widget( $table[0], $req_query );
        $act_monthly_count = sql_callback_for_dashboard_widget( $table[0], $act_month_cnt );
        $act_yearly_count = sql_callback_for_dashboard_widget( $table[0], $act_year_cnt );
        $act_total_count = sql_callback_for_dashboard_widget( $table[0], $act_total_cnt );
        $in_monthly_count = sql_callback_for_dashboard_widget( $table[0], $in_month_cnt );
        $in_yearly_count = sql_callback_for_dashboard_widget( $table[0], $in_year_cnt );
        $in_total_count = sql_callback_for_dashboard_widget( $table[0], $in_total_cnt );
        $monthly_revenue = sql_callback_for_dashboard_widget( $table, $month_rev );
        $yearly_revenue = sql_callback_for_dashboard_widget( $table, $year_rev );
        $total_revenue = sql_callback_for_dashboard_widget( $table, $total_rev );

    }


    return array(
                'recent' => $recent_app,
                'revenue' => array(
                                'month' => $monthly_revenue,
                                'year' => $yearly_revenue,
                                'total' => $total_revenue
                                ),
                'total_apps' => array(
                                    'act_month' => $act_monthly_count,
                                    'act_year' => $act_yearly_count,
                                    'act_total' => $act_total_count,
                                    'in_month' => $in_monthly_count,
                                    'in_year' => $in_yearly_count,
                                    'in_total' => $in_total_count
                                    ),
                'req' => $data,
                );
}


/* Add ACF Field by code to add mail lista  */
if( function_exists('acf_add_local_field_group') ):

    acf_add_local_field_group(array(
        'key' => 'group_602a02615fd9e',
        'title' => 'MailPoet Mail List Entry',
        'fields' => array(
            array(
                'key' => 'field_602a0281e636a',
                'label' => 'Enter List and IDs',
                'name' => 'mail_poet_list',
                'type' => 'textarea',
                'instructions' => '{ "newsletter_list" : "3", ["list name 2" : "list id 2"] }',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'maxlength' => '',
                'rows' => 6,
                'new_lines' => '',
            ),
        ),
        'location' => array(
            array(
                array(
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => 'acf-options',
                ),
            ),
        ),
        'menu_order' => 3,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => true,
        'description' => '',
    ));

endif;

/** Enum List Update function 
function badem_AdditionNewPlatform ($EnumeValue){
    global $wpdb;
    $Enumtbl = $wpdb->prefix.'app_users';
    $enum = $wpdb->get_results( "SHOW COLUMNS FROM $Enumtbl LIKE 'app_platform'" );
    foreach ($enum as $en_key) {
        preg_match('/enum\((.*)\)$/', $en_key->Type, $matches);
        $vals = explode(',', $matches[1]);
        $Enum_list = (str_replace("'","",$vals));
        array_push($Enum_list, $EnumeValue);
        $EnumVal = "'" . implode ( "', '", $Enumlist ) . "'";
    }
    $AddEnum = $wpdb->query( " ALTER TABLE $Enumtbl MODIFY COLUMN app_platform enum( $EnumVal ) " );
    if($AddEnum){
        echo 'Update Enum list successfully.';
    }else{
        echo 'Enum list not update =>'.$AddEnum;
    }
}*/

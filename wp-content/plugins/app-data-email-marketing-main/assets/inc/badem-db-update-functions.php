<?php 
/*
*
*   ***** Bitcot Apps Data & Email Marketing *****
*
*   Core Model or DataBase Functions
*   
*/
/* If this file is called directly, abort. */
if ( ! defined( 'WPINC' ) ) {die;} // end if

/*
 * Mailpoet Plugin Changes
 */

class app_updation_by_user{

    private $tname = 'app_users';

    public function app_data_update($data){
        global $wpdb;   
        $tbname = $wpdb->prefix.$this->tname;
        $response = array();
        if($data['store_id'] && $data['app_id']){

            $udp_row_data = $this->CheckIfEntryExist($data['store_id'], $data['app_id']);
            if($data['email']){
                if($udp_row_data){

                    $rwdata = array(
                                'app_status'=> $data['app_status'],
                                'app_status_update_at' => date('Y-m-d H:i:s')
                                );
                    $where = array(
                                'id'=> $udp_row_data->id
                                );

                    if( $data['app_status_update_at'] )
                    {
                        $rwdata['app_status_update_at'] = $data['app_status_update_at'];
                    }
                    
                    $update_rows = $wpdb->update( $tbname, $rwdata, $where );

                    if($update_rows){
                        $response['status'] = "success";
                        $response['msg'] = "Your row data updated done.";
                        $response['email'] = $udp_row_data->email;
                        return $response;
                    } else{
                        $response['status'] = "fail";
                        $response['msg'] = "Your data is not update.";
                        return $response;
                    }
                }else{
                   $response['status'] = "fail";
                   $response['msg'] = "Provide store & app data are not existing.";
                    return $response; 
                }
            }else{
                $response['status'] = "fail";
                $response['msg'] = "Email id is required.";
                return $response;
                }
        } else{
            $response['status'] = "fail";
            $response['msg'] = "Provide store and app data are required.";
            return $response;
        }

    }
    public function CheckIfEntryExist($store_id, $app_id){
        global $wpdb;
        $tname = 'app_users';
        $tbname = $wpdb->prefix.$tname;
        $sql = "SELECT * FROM $tbname WHERE store_id = '$store_id' AND app_id = '$app_id'";
        $sqex = $wpdb->get_row( $sql );
            
        if($sqex){
            return $sqex;
        }else{
            return false; 
        }
    }
}

new app_updation_by_user;
<?php

/*
*
*   ***** Bitcot Apps Data & Email Marketing *****
*
*   Core Model or DataBase Functions
*   
*/
/* If this file is called directly, abort. */
if ( ! defined( 'WPINC' ) ) {die;}  /*end if*/
/*
 * Mailpoet Plugin Changes
 */
class app_plan_subscription_function{

	public function apps_plan_subscriptions($data){
		global $wpdb;   
        $rcv_msg = array();

        $plan_matched = $this->plan_data_matched( $data );

        if( isset($plan_matched['status'] ) ){
            return $plan_matched;
        }else{

            $table_name = $wpdb->prefix.'badem_plans';
            $plan_array = array(
            				'store_id'		=>	$plan_matched['store_id'],
            				'app_id'		=>	$plan_matched['app_id'],
            				'app_name'		=>	$plan_matched['app_name'],
            				'trial_day'		=>	$plan_matched['trial_day'],
            				'plan_id'		=>	$plan_matched['plan_id'],
            				'plan_type'		=>	$plan_matched['plan_type'],
            				'price'			=>	$plan_matched['price'],
            				'payment_status'=>	$plan_matched['payment_status'],
            				'transaction_id'=>	$plan_matched['transaction_id'],
            				'plan_service'	=>	$plan_matched['plan_service']
            				);
            $add_plan_data = $wpdb->insert( $table_name, $plan_array);
            $plan_data_id = $wpdb->insert_id;
            if($plan_data_id){
            	$rcv_msg['status'] = 'success';
				$rcv_msg['mesg'] = "Application plan details are added successfully.";
				return $rcv_msg;
            }else{
            	$rcv_msg['status'] = 'Failed';
				$rcv_msg['mesg'] = "Plan data having trouble on inserting in table.";
				return $rcv_msg;
            }
        }
	}

	public function plan_data_matched( $data ){
		$app_plan_array = array();
        $rcv_msg = array();
        
		if(!empty($data['store_id']) && !empty($data['app_id']) && !empty($data['app_name'])){

			$plan_data = $this->validate_plan_info($data['store_id'], $data['app_id'], $data['app_name']);

			if($plan_data['ret']){

				$app_plan_array['store_id'] = $plan_data['id'];
				$app_plan_array['app_id'] = $data['app_id'];
				$app_plan_array['app_name'] = $data['app_name'];
			}else{
				$rcv_msg['status'] = 'failed';
				$rcv_msg['mesg'] = "Store & app data not exist in our data.";
			}
		}else{
			$rcv_msg['status'] = 'failed';
			$rcv_msg['mesg'] = "rewrite the store and app data details.";
		}
		
		if(!empty($data['trial_day'])){
			$app_plan_array['trial_day'] = $data['trial_day'];
		}else{
			$rcv_msg['status'] = 'failed';
			$rcv_msg['mesg'] = "Provide trial period days.";
		}

		if(!empty($data['plan_id'])){
			$app_plan_array['plan_id'] = $data['plan_id'];
		}else{
			$rcv_msg['status'] = 'failed';
			$rcv_msg['mesg'] = "This plan is not created by us.";
		}

		if($data['plan_type'] == 'recurring' || $data['plan_type'] == 'one-time-paid'){
			$app_plan_array['plan_type'] = $data['plan_type'];
		}else{
			$rcv_msg['status'] = 'failed';
			$rcv_msg['mesg'] = "Provide specific method for plan type.";
		}

		if(!empty($data['price'])){
			$app_plan_array['price'] = $data['price'];
		}else{
			$rcv_msg['status'] = 'failed';
			$rcv_msg['mesg'] = "Plan price is not define in the data.";
		}

		if($data['payment_status'] == 'completed' || $data['payment_status'] == 'failed' || $data['payment_status'] == 'under-process' ){
			$app_plan_array['payment_status'] = $data['payment_status'];
		}else{
			$rcv_msg['status'] = 'failed';
			$rcv_msg['mesg'] = "Please update payment status in format.";
		}

		if(!empty($data['transaction_id'])){
			$app_plan_array['transaction_id'] = $data['transaction_id'];
		}else{
			$rcv_msg['status'] = 'failed';
			$rcv_msg['mesg'] = "Given to transaction id for track payment status.";
		}

		if($data['plan_service'] == 'free' || $data['plan_service'] == 'paid' || $data['plan_service'] == 'trial'){
			$app_plan_array['plan_service'] = $data['plan_service'];
		}else{
			$rcv_msg['status'] = 'failed';
			$rcv_msg['mesg'] = "App plan service not define.";
		}

		if(!empty($rcv_msg)){
			return $rcv_msg;
		}else{
			return $app_plan_array;
		}
	}

	public function validate_plan_info($storeid, $appid, $appname){
		global $wpdb;

		$app_table = $wpdb->prefix.'app_users';
		$plan_info = $wpdb->get_row( "SELECT id, store_id, app_id, app_name FROM 
									$app_table WHERE store_id = '$storeid' AND app_id = '$appid' AND app_name = '$appname'" );
		$plan_data = array();
		
		if($plan_info){
			$plan_data['id'] = $plan_info->id;
			$plan_data['ret'] = true;
			return $plan_data;
		}else{
			$plan_data['ret'] = false;
			return $plan_data;
		}
	}
}
new app_plan_subscription_function;
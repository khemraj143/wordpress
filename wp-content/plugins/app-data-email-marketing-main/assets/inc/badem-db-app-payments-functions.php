<?php
/*
*
*   ***** Bitcot Apps Data & Email Marketing *****
*
*   Core Model or DataBase Functions
*   
*/
/* If this file is called directly, abort. */
if ( ! defined( 'WPINC' ) ) {die;}  /*end if*/
/*
 * Mailpoet Plugin Changes
 */
 class badem_get_app_payments{

 	public function app_payments_processing_data( $data ){
 		global $wpdb;   
        $array_app_paid = array();
        $error_msg = array();

        $paid_data = $this->payments_data_checks( $data );
        if( isset($paid_data['status']) && !empty($paid_data['status'])){
        	return $paid_data;
        }else{

        	$app_pay_table = $wpdb->prefix.'badem_payments';
        	$insert_payments = array(
				'store_id' => $paid_data['store_id'],
				'app_id' => $paid_data['app_id'],
				'start_at' => $paid_data['start_at'],
				'expired_on' => $paid_data['expired_on'],
				'schedule_date' => $paid_data['schedule_date'],
				'payment_status'=> $paid_data['payment_status'],
                                'transaction_id' => $paid_data['transaction_id'],
				'amount' => $paid_data['amount'],
                                'currency' => $paid_data['currency']
				);

                $data['created_at'] = '';
                if( $data['created_at'] )
                {
                        $insert_payments['created_at'] = $data['created_at'];
                }else
                {
                      $insert_payments['created_at'] = date('Y-m-d H:i:s');  
                }
        	$add_payments = $wpdb->insert( $app_pay_table, $insert_payments );
        	$done_payments = $wpdb->insert_id;
        	if( $done_payments ){
        		$error_msg['status'] = 'success';
 				$error_msg['emsg'] = 'Your payment has been received successfully.';
 				return $error_msg;
        	}else{
        		$error_msg['status'] = 'Failed';
 				$error_msg['emsg'] = 'Your payment has failed due to some technical errors.';
 				return $error_msg;
        	}
        }
 	}

 	public function payments_data_checks( $pdata ){
 		$array_app_paid = array();
        $error_msg = array();

 		if( !empty($pdata['store_id']) && !empty($pdata['app_id']) ){
 			$appData_verify = $this->apps_data_verified($pdata['store_id'], $pdata['app_id']);
 			if( $appData_verify['verify'] ){
 				$array_app_paid['store_id'] = $appData_verify['id'];
 				$array_app_paid['app_id'] = $pdata['app_id'];
 			}else{
 				$error_msg['status'] = 'Failed';
 				$error_msg['emsg'] = 'Store and app data do not exist in the data.';
 			}
 		}else{
 			$error_msg['status'] = 'Failed';
 			$error_msg['emsg'] = 'Store and app data required for your payments.';
 		}

 		if($pdata['start_at']){
 			$array_app_paid['start_at'] = $pdata['start_at'];
 		}else{
 			$error_msg['status'] = 'Failed';
 			$error_msg['emsg'] = 'Apps subscription starting date not received.';	
 		}

 		if($pdata['expired_on']){

 			$array_app_paid['expired_on'] = $pdata['expired_on'];
 		}else{
 			$error_msg['status'] = 'Failed';
 			$error_msg['emsg'] = 'Apps subscription expired date not received.';	
 		}

 		if($pdata['schedule_date']){

 			$array_app_paid['schedule_date'] = $pdata['schedule_date'];
 		}else{
 			$error_msg['status'] = 'Failed';
 			$error_msg['emsg'] = 'Next payment date has not received.';	
 		}

 		if($pdata['payment_status'] == 'paid' || $pdata['payment_status'] == 'failed' || $pdata['payment_status'] == 'in-process'){
 			$array_app_paid['payment_status'] = $pdata['payment_status'];
 		}else{
 			$error_msg['status'] = 'Failed';
 			$error_msg['emsg'] = 'Payement status does not in our format.';	
 		}

                if(isset($pdata['transaction_id']) || !empty($pdata['transaction_id'])){
                        $array_app_paid['transaction_id'] = $pdata['transaction_id'];
                }else{
                        $error_msg['status'] = 'Failed';
                        $error_msg['emsg'] = 'Provide transaction details for track your payments.';    
                }

 		if($pdata['amount']){
 			$array_app_paid['amount'] = $pdata['amount'];
 		}else{
 			$error_msg['status'] = 'Failed';
 			$error_msg['emsg'] = 'Please enter paid amount for payment done.';	
 		}

                if($pdata['currency']){
                        $array_app_paid['currency'] = $pdata['currency'];
                }else{
                        $error_msg['status'] = 'Failed';
                        $error_msg['emsg'] = 'Please provide currency name.';
                }

 		if(isset($error_msg) && !empty($error_msg)){
 			return $error_msg;
 		}else{
			return $array_app_paid;
 		}
 	}

 	public function apps_data_verified($av_store_id, $av_app_id){
 		global $wpdb;
 		$av_table = $wpdb->prefix.'app_users';
 		$av_users = $wpdb->get_row( "SELECT id, store_id, app_id FROM $av_table WHERE
 									store_id = '$av_store_id' AND app_id = '$av_app_id'" );
 		$ver_data = array();
 		if( $av_users ){
 			$ver_data['id'] = $av_users->id;
 			$ver_data['verify'] = true;
 			return $ver_data;
 		}else{
 			$ver_data['verify'] = false;
 			return $ver_data;
 		}
 	}
 }
 new badem_get_app_payments;
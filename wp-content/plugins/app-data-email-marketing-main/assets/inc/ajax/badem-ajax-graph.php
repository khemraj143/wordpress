<?php

/*****
	Description: Ajax Handler file for data fetch by database.
	Author: BitCot
	Created: 23-Mar-2021
	Modify: 06-apr-2021
*****/
add_action( 'wp_ajax_stat_graph_data', 'badem_statistics_graph_function' );

function badem_statistics_graph_function()
{
	$post = $_POST;

	if( $post['rev']['id'] == '#stat_graph' )
	{
		$element = $post['rev']['id'];

		$rev_filter = array();

		if( $post['filter'] )
		{
			if( $post['filter']['year'] && $post['filter']['attr'] == 'year' )
			{
				$year = $post['filter']['year'];
				$year_date = substr($year,4,11);
				$rev_filter['colunm'] = "MONTH( pt.start_at ) as month, YEAR( pt.start_at ) as year, SUM( pt.amount ) as amount";
				$rev_filter['group'] = "GROUP BY month, year";
				$rev_filter['fltr_year'] = date('Y-m-d', strtotime($year_date));
			}
			
			if( $post['filter']['half_year'] && $post['filter']['attr'] == 'half_year' )
			{
				$half_year = $post['filter']['half_year'];
				$half_year_date = substr($half_year,4,11);
				$rev_filter['colunm'] = "MONTH( pt.start_at ) as month, YEAR( pt.start_at ) as year, SUM( pt.amount ) as amount";
				$rev_filter['group'] = "GROUP BY month, year";
				$rev_filter['fltr_half_year'] = date('Y-m-d', strtotime($half_year_date));
			}

			if( $post['filter']['quarter'] && $post['filter']['attr'] == 'three_month' )
			{
				$quarter = $post['filter']['quarter'];
				$quarter_date = substr($quarter,4,11);
				$rev_filter['colunm'] = "MONTH( pt.start_at ) as month, YEAR( pt.start_at ) as year, SUM( pt.amount ) as amount";
				$rev_filter['group'] = "GROUP BY month, year";
				$rev_filter['fltr_quarter'] = date('Y-m-d', strtotime($quarter_date));
			}

			if( $post['filter']['week'] && $post['filter']['attr'] == 'week' )
			{
				$week = $post['filter']['week'];
				$week_date = substr($week,4,11);
				$rev_filter['colunm'] = "pt.start_at as day, pt.amount";
				$rev_filter['group'] = "";
				$rev_filter['fltr_week'] = date('Y-m-d', strtotime($week_date));			
			}

			if( $post['filter']['from_filter'] && $post['filter']['from_id'] == '#from_filter' )
			{
				$rev_filter['colunm'] = "MONTH( pt.start_at ) as month, YEAR( pt.start_at ) as year, SUM( pt.amount ) as amount";
				$rev_filter['group'] = "GROUP BY month, year";
				$rev_filter['from_filter'] = $post['filter']['from_filter'];
			}

			if( $post['filter']['to_filter'] && $post['filter']['to_id'] == '#to_filter' )
			{
				$rev_filter['colunm'] = "MONTH( pt.start_at ) as month, YEAR( pt.start_at ) as year, SUM( pt.amount ) as amount";
				$rev_filter['group'] = "GROUP BY month, year";
				$rev_filter['to_filter'] = $post['filter']['to_filter'];
			}
		}else
		{
			$rev_filter['colunm'] = "MONTH( pt.start_at ) as month, YEAR( pt.start_at ) as year, SUM( pt.amount ) as amount";
			$rev_filter['group'] = "GROUP BY month, year";
		}

		$rev_graph = get_data_by_sql( $rev_filter, $element );
	}


	if( $post['usr']['id'] == '#user_graph' )
	{
		$element = $post['usr']['id'];

		$usr_filter = array();

		if( $post['filter'] )
		{
			if( $post['filter']['year'] && $post['filter']['attr'] == 'ap_year' )
			{
				$ap_year = $post['filter']['year'];
				$ap_date = substr($ap_year,4,11);
				$usr_filter['colunm'] = "MONTH(created_at) as month, YEAR(created_at) as year, COUNT(app_status) as app";
				$usr_filter['group'] = "GROUP BY month, year";
				$usr_filter['fltr_year'] = date('Y-m-d', strtotime($ap_date));
			}
			
			if( $post['filter']['half_year'] && $post['filter']['attr'] == 'ap_half' )
			{
				$ap_half = $post['filter']['half_year'];
				$ap_half_date = substr($ap_half,4,11);
				$usr_filter['colunm'] = "MONTH(created_at) as month, YEAR(created_at) as year, COUNT(app_status) as app";
				$usr_filter['group'] = "GROUP BY month, year";
				$usr_filter['fltr_half_year'] = date('Y-m-d', strtotime($ap_half_date));
			}

			if( $post['filter']['quarter'] && $post['filter']['attr'] == 'ap_quarter' )
			{
				$ap_quarter = $post['filter']['quarter'];
				$ap_quarter_date = substr($ap_quarter,4,11);
				$usr_filter['colunm'] = "MONTH(created_at) as month, YEAR(created_at) as year, COUNT(app_status) as app";
				$usr_filter['group'] = "GROUP BY month, year";
				$usr_filter['fltr_quarter'] = date('Y-m-d', strtotime($ap_quarter_date));
			}

			if( $post['filter']['week'] && $post['filter']['attr'] == 'ap_week' )
			{
				$ap_week = $post['filter']['week'];
				$ap_week_date = substr($ap_week,4,11);
				$usr_filter['colunm'] = "created_at as day, COUNT(app_status) as app";
				$usr_filter['group'] = "";
				$usr_filter['fltr_week'] = date('Y-m-d', strtotime($ap_week_date));			
			}

			if( $post['filter']['from_filter'] && $post['filter']['from_id'] == '#ap_from_filter' )
			{
				$usr_filter['colunm'] = "MONTH(created_at) as month, YEAR(created_at) as year, COUNT(app_status) as app";
				$usr_filter['group'] = "GROUP BY month, year";
				$usr_filter['from_filter'] = $post['filter']['from_filter'];
			}

			if( $post['filter']['to_filter'] && $post['filter']['to_id'] == '#ap_to_filter' )
			{
				$usr_filter['colunm'] = "MONTH(created_at) as month, YEAR(created_at) as year, COUNT(app_status) as app";
				$usr_filter['group'] = "GROUP BY month, year";
				$usr_filter['to_filter'] = $post['filter']['to_filter'];
			}
		}else
		{
			$usr_filter['colunm'] = "MONTH(created_at) as month, YEAR(created_at) as year, COUNT(app_status) as app";
			$usr_filter['group'] = "GROUP BY month, year";
		}

		$user_graph = get_data_by_sql( $usr_filter, $element );
	}

	if( $rev_graph['data'] && $rev_graph['rid'] == '#stat_graph' )
	{
		$gen_data = array();
		$get_data = $rev_graph['data'];
		if( !$post['filter']['week'] )
		{
			foreach ( $get_data as $key => $value )
			{	/*date( 'F', mktime( 0, 0, 0, $value->month, 10 ) ).'-'.$value->year*/
				/*date( 'Y-m-d', mktime( 0, 0, 0, $value->month+1,0,$value->year ) )*/
				if( $value->month && $value->year && $value->amount )
				{
					$x_value = date( 'Y-m-d H:i:s', mktime( 0, 0, 0, $value->month+1,0,$value->year ) );
					$y_value = $value->amount;
					$gen_data[] = array($x_value, (float)$y_value);
				}
			}
		}else
		{
			foreach ( $get_data as $key => $value )
			{
				if( $value->day && $value->amount )
				{
					$x_value = $value->day;
					$y_value = $value->amount;
					$gen_data[] = array($x_value, (float)$y_value);
				}
			}
		}

		$rev_data = array(
					'graph_id' => $rev_graph['rid'],
					'graph' => $gen_data,
					'graph_query' => $rev_graph['sql_query'],
					'response' => $post['rev'],
					'filter' => $post['filter']['attr']
					);
	}

	if( $user_graph['data'] && $user_graph['rid'] == '#user_graph' )
	{
		$new_data = array();
		$get_user = $user_graph['data'];

		if( !$post['filter']['week'] )
		{
			foreach ( $get_user as $key => $value )
			{
				if( $value->month && $value->year && $value->app )
				{	
					$x_value = date( 'Y-m-d H:i:s', mktime( 0, 0, 0, $value->month+1,0,$value->year ) );
					$y_value = $value->app;
					$new_data[] = array($x_value, (int)$y_value);
				}
			}
		}else
		{
			foreach ( $get_user as $key => $value )
			{
				if( $value->day && $value->app )
				{
					$x_value = $value->day;
					$y_value = $value->app;
					$new_data[] = array($x_value, (int)$y_value);
				}
			}
		}

		$usr_data = array(
					'graph_id' => $user_graph['rid'],
					'graph' => $new_data,
					'graph_query' => $user_graph['sql_query'],
					'response' => $post['usr'],
					'filter' => $post['filter']
					);
	}

	echo json_encode(array(
					'rev_id' => $rev_data['graph_id'],
					'rev_graph' => $rev_data['graph'],
					'rev_query' => $rev_data['graph_query'],
					'rev_res' => $rev_data['response'],
					'usr_id' => $usr_data['graph_id'],
					'usr_graph' => $usr_data['graph'],
					'usr_query' => $usr_data['graph_query'],
					'usr_res' => $usr_data['response'],
					'test' => $get_user
					));
		wp_die();
	
}

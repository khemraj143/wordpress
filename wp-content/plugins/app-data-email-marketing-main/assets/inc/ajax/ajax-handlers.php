<?php
/*****
	Description: Ajax handlers file for handling js code without page refresh.
	Author: BitCot
	Created: 30-Mar-2021
	Modify:

*****/


add_action( 'wp_ajax_get_apps_data_table', function ()
{
	global $wpdb;
	$req = $_GET;
	$db_table = 'apps_platform';
	$data_array = array();
	$others = app_datatable_calback( $db_table, $req );
 
	$table_columns = '';
        if( $others['dir_request'] )
        {
            $table_columns = $others['dir_request']['table_columns'];
        }
	

	$row_no = 1;
        foreach ($others['results'] as $key => $value) {

            $data_row = array();
            
            for ($i=0; $i < count($table_columns); $i++) { 
                
                $table_key = $table_columns[$i];
                $id = $table_columns[0];

                switch( $table_key )
                {
                    case 'action' :
                    $data_row[$table_key] = "<a id='row_data_id' data-row=".$value->$id." class='btn '><span class='dashicons dashicons-edit'></span></a>"; 
                    break;

                    default :
                    if( $value->$table_key == 'big-commerce' )
                    {
                    	$value->$table_key = 'BigCommerce';	
                    }
                    if( $value->$table_key == 'shopify' )
                    {
                    	$value->$table_key = 'Shopify';	
                    }
                    $data_row[$table_key] = $value->$table_key;
                    break;
                }

            }
            $row_no++;
            $data_array[] = $data_row;

        }

	echo json_encode(array(
					'data' => $data_array,
					'total' => $others['total'],
					'other' => $others['queries'],
					'requst' => $others,
					'cols' => $table_columns
					) );
	wp_die();
} );

add_action( 'wp_ajax_edit_app_data', function()
{
	$data_id = $_POST['data_id'];
	$update_app_data = get_all_data_form_database( 'apps_db' );
	
	foreach ($update_app_data as $key => $value) {
		if( $value->id == $data_id )
		{
			$app_array = $value;
		}
	}

	echo json_encode(array(
					'apps' => $app_array
					) );
	wp_die();
} );
<?php

/*****
	Description: Ajax handlers file for handling js code without page refresh.
	Author: BitCot
	Created: 30-Mar-2021
	Modify: 01-Apr-2021

*****/

add_action('wp_ajax_platforms', 'badem_fetch_app_data');
function badem_fetch_app_data()
{

	$post = $_POST;
	$dash_panel = dashboard_widget_data( $post );
	$apps = selection_app_lists( $post );

	$platform = $apps['get_data'];
	ob_start();
	if( $platform )
	{
		?>
		<option value=""> All platform </option>
		<?php
		foreach ( $platform as $key => $value )
		{
	        ?>
	            <option value="<?php echo $value->slug; ?>" <?php if( $value->slug == $post['fltr_platform'] ){echo 'selected';}?> > <?php echo $value->platform; ?> </option>
	            
	        <?php
		}

	}else{
		?>
		<option value=""> No list found </option>
		<?php
	}
	$dd_platform = ob_get_clean();

	if( $dash_panel['recent']['results'] )
	{
		ob_start();
		$recent_data = $dash_panel['recent']['results'];
		?>
		<tr>
			<td>
				<table class="table" style="width: 100%;">
					<?php
                    foreach ($recent_data as $key => $value) {
                        if($value->app_platform == 'big-commerce' && $value->app_status == 'active' )
                        {
                        ?>
						<tr>
							<td>
								<a href="<?php echo $value->store_url; ?>">
                                    <?php echo $value->store_name; ?>
                                </a>
                                 ( <?php echo $value->app_name; ?> )
                                <strong>Email:</strong> <?php echo $value->email; ?></br>
							</td>
						</tr>
						<?php
                        }
                    }
                ?>
				</table>
			</td>

			<td>
				<table class="table" style="width: 100%;">
					<?php
                    foreach ($recent_data as $key => $value) {
                        if($value->app_platform == 'big-commerce' && $value->app_status == 'inactive' )
                        {
                        ?>
						<tr>
							<td>
								<a href="<?php echo $value->store_url; ?>">
                                    <?php echo $value->store_name; ?>
                                </a>
                                 ( <?php echo $value->app_name; ?> )
                                <strong>Email:</strong> <?php echo $value->email; ?></br>
							</td>
						</tr>
						<?php
                        }
                    }
                ?>
				</table>
			</td>

			<td>
				<table class="table" style="width: 100%;">
					<?php
                    foreach ($recent_data as $key => $value) {
                        if($value->app_platform == 'shopify' && $value->app_status == 'active' )
                        {
                        ?>
						<tr>
							<td>
								<a href="<?php echo $value->store_url; ?>">
                                    <?php echo $value->store_name; ?>
                                </a>
                                 ( <?php echo $value->app_name; ?> )
                                <strong>Email:</strong> <?php echo $value->email; ?></br>
							</td>
						</tr>
						<?php
                        }
                    }
                ?>
				</table>
			</td>

			<td>
				<table class="table" style="width: 100%;">
					<?php
                    foreach ($recent_data as $key => $value) {
                        if($value->app_platform == 'shopify' && $value->app_status == 'inactive' )
                        {
                        ?>
						<tr>
							<td>
								<a href="<?php echo $value->store_url; ?>">
                                    <?php echo $value->store_name; ?>
                                </a>
                                 ( <?php echo $value->app_name; ?> )
                                <strong>Email:</strong> <?php echo $value->email; ?></br>
							</td>
						</tr>
						<?php
                        }
                    }
                ?>
				</table>
			</td>
		</tr>
		<?php
	$content = ob_get_clean();
	}

	echo json_encode( array( 
							'app_list' => $apps['content'],
							'tables' => $content,
							'apps_total' => $dash_panel['total_apps'],
							'total_revenue' => $dash_panel['revenue'],
							'other' => $dash_panel,
							'show_app' => $content,
							'show_pform' => $dd_platform,
							'all' => $apps
						) );
	wp_die();

}

add_action('wp_ajax_badem_paid_information', 'badem_admin_manage_pay_info');
function badem_admin_manage_pay_info(){

	global $wpdb;
    $request= $_GET;
    $json_data = array();
    $table_name = $wpdb->prefix . 'app_users';
    $filterMode = get_option( 'data_access_mode' );

    $table = array( 'app_users', 'badem_payments' );
    $table_data = app_datatable_multitable_callback( $table, $request );

    $results = $table_data['results'];

	ob_start();
    if($results){

    	$app = '';
		if( $request['fltr_app'] )
		{
			$app = $request['fltr_app'];
		}

		?>
		<option value=""> All Apps </option>
		<?php
		foreach ($results as $key => $value) {
			?>
			<option value="<?php echo $value->app_id; ?>" <?php if($value->app_id == $app): echo selected; endif; ?> >
				 <?php echo $value->app_name; ?>
			</option>

			<?php
		}
		
	}else
	{
		?>
		<option value=""> No Apps </option>
		<?php
	}
	$content = ob_get_clean();

    if( $results ){
        $nums = 1;

        foreach ($results as $QRkey => $QRreq)
        {
            $row = array();
	        $row['app_id'] = $QRreq->app_id;
	        $row['app_name'] = $QRreq->app_name;
	        $row['name'] = $QRreq->name;
	        $row['email'] = $QRreq->email;
	        $row['start_at'] = $QRreq->start_at;
	        $row['expired_on'] = $QRreq->expired_on;
	        $row['payment_status'] = strtoupper($QRreq->payment_status);
	        $row['amount'] =$QRreq->amount.' '.strtoupper($QRreq->currency);
	        $row['schedule_date'] = $QRreq->schedule_date;
            
            $json_data[] = $row;
            $nums++;
        }
    }

    echo json_encode(array(
    				'data' => $json_data,
    				'total' => $table_data['total'],
    				'query' => $table_data['queries'],
    				'common_req' => $table_data['dir_request'],
    				'filter_req' => $table_data['flr_request'],
    				'apps' => $content
    				 ));
    wp_die();

}

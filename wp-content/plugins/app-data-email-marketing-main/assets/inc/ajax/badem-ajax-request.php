<?php 

/*****
    Description: Ajax handlers file for handling js code without page refresh.
    Author: BitCot
    Created: 30-Mar-2021
    Modify: 01-Apr-2021

*****/

/*If this file is called directly, abort.*/
if ( ! defined( 'WPINC' ) ) {die;}  /*end if*/
/*
Ajax Requests
*/
add_action( 'wp_ajax_badem_custom_plugin_frontend_ajax', 'badem_custom_plugin_frontend_ajax' );
add_action( 'wp_ajax_nopriv_badem_custom_plugin_frontend_ajax', 'badem_custom_plugin_frontend_ajax' );
function badem_custom_plugin_frontend_ajax(){   
    if(isset($_GET['id'])){
        global $wpdb;
        $table_name = $wpdb->prefix . 'app_users';
        $dataModes = get_option( 'data_access_mode' );
        $row_id = $_GET['id'];
        $query = "SELECT * FROM $table_name t1 WHERE environment = '$dataModes' AND t1.id = '$row_id'";
        $Results = $wpdb->get_row( $query );
        ob_start();
        ?>
        <div class="row">
            <?php
            $i = 1;
            foreach ($Results as $key => $value)
            {
                if( $key != 'environment' )
                {

                ?>
                <div class="col-sm-6">
                    <div class="content-card">
                        <p><strong><?php echo str_replace('_', ' ', ucfirst($key)); ?></strong> : <?php echo $value; ?></p>
                    </div>
                </div>
                <?php
                }
                $i++;
            }
            ?>
        </div>
        <?php
        $html = ob_get_clean();
        echo $html;
    } else {
        /*Your ajax Request & Response*/
        echo 'No user selected';    
    } 
    wp_die();
}

/* 
* List all apps as per platform
*/
add_action( 'wp_ajax_get_appnames_list_for_datatables', 'badem_get_appnames_list_for_datatables' );
function badem_get_appnames_list_for_datatables(){
    global $wpdb;
    $apps_table = $wpdb->prefix.'badem_apps';
    $platforms_table = $wpdb->prefix.'badem_platforms';
    $dataModes = get_option( 'data_access_mode' );
    if( isset( $_GET['pf'] ) && !empty( $_GET['pf'] ) ){
        $platform = $_GET['pf'];
        $sql = "SELECT * FROM $apps_table ap LEFT JOIN $platforms_table pt ON ap.platform = pt.id WHERE ap.environment = '$dataModes' AND pt.slug = '$platform'";
        $platforms = $wpdb->get_results($sql);
        $options = '';
        if($platforms){
            $options .= '<option value="">All Apps</option>';
            foreach ($platforms as $key => $value) {
                $options .= '<option value="'.$value->app_id.'">'.$value->app_name.'</option>';
            }
        }else{
            $options .= '<option value="">No apps for this platform</option>';
        }
        echo $options;
    }else{
        $sql = "SELECT * FROM $apps_table ap LEFT JOIN $platforms_table pt ON ap.platform = pt.id WHERE ap.environment = '$dataModes'";
        $platforms = $wpdb->get_results($sql);
        $options = '';
        if($platforms){
            $options .= '<option value="">All Apps</option>';
            foreach ($platforms as $key => $value) {
                $options .= '<option value="'.$value->app_id.'">'.$value->app_name.'</option>';
            }
        }else{
            $options .= '<option value="">No apps for this platform</option>';
        }
        echo $options;
    }
    wp_die();
}

/* 
* List all users send ajax response
*/
add_action( 'wp_ajax_get_appusers_list_for_datatables', 'badem_get_appusers_list_for_datatables' );
function badem_get_appusers_list_for_datatables(){   
    global $wpdb;
    $request= $_GET;

    $return_json = array();

    $table_data = app_datatable_calback( 'app_users', $request );

    $results = $table_data['results'];

    $apps_filter = selection_app_lists($request);

    $platform = $apps_filter['get_data'];
    ob_start();
    if( $platform )
    {
        ?>
        <option value=""> All platform </option>
        <?php
        foreach ( $platform as $key => $value )
        {
            ?>
                <option value="<?php echo $value->slug; ?>" <?php if( $value->slug == $request['fltr_platform'] ){echo 'selected';}?> > <?php echo $value->platform; ?> </option>
                
            <?php
        }

    }else{
        ?>
        <option value=""> No list found </option>
        <?php
    }
    $dd_platform = ob_get_clean();
    /*ob_start();
    if($results){
        
        $app = '';
        if( $request['fltr_app'] )
        {
            $app = $request['fltr_app'];
        }

        ?>
        <option value=""> All Apps </option>
        <?php
        $app_name = array();
        foreach ($results as $key => $value)
            $app_name['app_name'] = $value->app_name;
            $app_name = array_unique($app_name);
            ?>
            <option value="<?php echo $value->id; ?>" <?php if($app_name['id'] == $app): echo selected; endif; ?> >
                 <?php echo $app_name['app_name']; ?>
            </option>

            <?php
    }else
    {
        ?>
        <option value=""> No Apps </option>
        <?php
    }
    $content = ob_get_clean();*/
    

    if(count($results) > 0){
        
        foreach ($results as $key => $req) {
            if( $req->app_status == 'active'){
                $astatus = 'Install';
            }else if( $req->app_status == 'inactive'){
                $astatus = 'Uninstall';
            }
            $row = array();
            
            $row['id'] = $req->id;
            $row['store_id'] = $req->store_id;
            $row['app_id'] = $req->app_id;
            $row['store_name'] = $req->store_name;
            $row['app_name'] = $req->app_name;
            $row['name'] = $req->name;
            $row['email'] = $req->email;
            $row['app_status'] = $astatus;
            $row['created_at'] = $req->created_at;
            $row['current_app_plan'] = strtoupper($req->current_app_plan);
            $row['mail_lists'] = '<div><a href="javascript:show_quick_user_view('.$req->id.');" class="btn btn_primary">Quick view </a>&nbsp;<span class="button full_view d-none"><a href="#'.$req->id.'"> Full view</a></span></div>'; 
            $return_json[] = $row;
        }
    }


    $totalData = count($totalRequests);

    echo json_encode(array( 'data' => $return_json,
                            'total' => $table_data['total'],
                            'query' => $table_data['queries'],
                            'type' => $request,
                            'others' => $table_data,
                            'filter_app' => $apps_filter['content'],
                            'platforms' => $dd_platform
                            ));
    wp_die();
}

